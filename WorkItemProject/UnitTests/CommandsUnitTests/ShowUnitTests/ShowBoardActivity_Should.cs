﻿using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.ShowCommands;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using UnitTests.BaseUnitTest;

namespace UnitTests.UnitTests
{
    [TestClass]
    public class ShowBoardActivity_Should : BaseTest
    {
        [TestMethod]

        public void ShowBoardActivityValid_Should()
        {
            string boardName = "boardGames";
            string teamName = "Jumanji";

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            EventHistory history = new EventHistory($"Board with the name: {boardName} was created.");
            IList<string> commands = new List<string> { boardName, teamName };
            Command command = new BoardActivityVisualizer(commands);
            
            DataBase.AddTeam(team);
            DataBase.AddBoard(board,teamName);

            board.History.Add(history);
            string actual = command.Execute();

            Assert.AreEqual(history.ToString(), actual);
            Assert.IsNotNull(board);
        }
        [TestMethod]
        public void ShowBoardActivityInvalidParameters_Should()
        {
            string boardName = "boardGames";

            IList<string> commands = new List<string> { boardName };
            Command command = new BoardActivityVisualizer(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowBoardActivityBoardNameIsNULL_Should()
        {
            string boardName = null;
            string teamName = "Jumanji";

            Team team = new Team(teamName);
            DataBase.AddTeam(team);

            IList<string> commands = new List<string> { boardName, teamName };
            Command command = new BoardActivityVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowBoardActivityTeamNameIsNULL_Should()
        {
            string boardName = "boardGames";
            string teamName = null;

            IList<string> commands = new List<string> { boardName, teamName };
            Command command = new BoardActivityVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}