﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ShowAllPeople_Should : BaseTest
    {
        [TestMethod]
        public void ValidShowAllPeople_Should()
        {
            string member = "tomas";

            Member memberOne = new Member(member);

            DataBase.AddMember(memberOne);

            IList<string> commands = new List<string>();
            Command command = new MembersVisualizer(commands);

            string expected = memberOne.ToString().Trim();
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InValidShowAllTeam_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new MembersVisualizer(commands);
            string expected = "There are no registered members.";
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
