﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ShowTeamBoard_Should : BaseTest
    {
        [TestMethod]
        public void ValidShowTeamBoards_Should()
        {
            string teamName = "teamninjas";
            string boardName = "validboard";
            Team team = new Team(teamName);
            Board board = new Board(boardName);

            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            IList<string> commands = new List<string> { teamName };
            Command command = new TeamBoardsVisualizer(commands);

            string actual = command.Execute();
            string expected = board.Name;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void ShowTeamBoardInvalidParameters_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new TeamBoardsVisualizer(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowTeamBoardNameIsNULL_Should()
        {
            string teamName = null;

            IList<string> commands = new List<string> { teamName };
            Command command = new TeamBoardsVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowTeamBoardNameIsEmpty_Should()
        {
            string teamName = string.Empty;

            IList<string> commands = new List<string> { teamName };
            Command command = new TeamBoardsVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowTeamBoardTeamIsNULL_Should()
        {
            string teamName = "teamninjas";

            IList<string> commands = new List<string> { teamName };
            Command command = new TeamBoardsVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidShowTeamBoards_Should()
        {
            string teamName = "teamninjas";
            Team team = new Team(teamName);


            DataBase.AddTeam(team);
            IList<string> commands = new List<string> { teamName };
            Command command = new TeamBoardsVisualizer(commands);

            string actual = command.Execute();
            string expected = "Team doesn't have any boards.";

            Assert.AreEqual(expected, actual);
        }
    }
}