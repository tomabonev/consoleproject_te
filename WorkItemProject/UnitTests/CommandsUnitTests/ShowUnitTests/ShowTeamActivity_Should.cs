﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.TeamCommands;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.UnitTests
{
    [TestClass]
    public class ShowTeamActivity_Should : BaseTest
    {
        [TestMethod]

        public void ShowTeamActivityValid_Should()
        {
            string teamName = "Jumanji";
            Team team = new Team(teamName);

            EventHistory history = new EventHistory($"Team with the name: { teamName } was created.");
            IList<string> commands = new List<string> { teamName };
            Command command = new TeamActivityVisualizer(commands);

            DataBase.AddTeam(team);

            team.History.Add(history);
            string actual = command.Execute();

            Assert.AreEqual(history.ToString(), actual);
            Assert.IsNotNull(team);
        }
        [TestMethod]
        public void ShowTeamActivityInvalidParameters_Should()
        {
            string teamName = "Jumanji";
            string memberName = "tomas";

            Team team = new Team(teamName);
            DataBase.AddTeam(team);

            IList<string> commands = new List<string> { memberName, teamName };
            Command command = new TeamActivityVisualizer(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowTeamActivityNameIsNULL_Should()
        {
            string teamName = null;

            IList<string> commands = new List<string> { teamName};
            Command command = new TeamActivityVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}