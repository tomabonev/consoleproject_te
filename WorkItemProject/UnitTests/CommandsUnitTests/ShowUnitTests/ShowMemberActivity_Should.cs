﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.UnitTests
{
    [TestClass]
    public class ShowMemberActivity_Should : BaseTest
    {
        [TestMethod]

        public void ShowMemberActivityValid_Should()
        {
            string memberName = "tomas";
            Member member = new Member(memberName);

            EventHistory history = new EventHistory($"Member with the name: { memberName } was created.");
            IList<string> commands = new List<string> { memberName };
            Command command = new MemberActivityVisualizer(commands);

            DataBase.AddMember(member);

            member.History.Add(history);
            string actual = command.Execute();

            Assert.AreEqual(history.ToString(), actual);
            Assert.IsNotNull(member);
        }
        [TestMethod]
        public void ShowMemberActivityInvalidParameters_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new MemberActivityVisualizer(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowMemberActivityNameIsNULL_Should()
        {
            string memberName = null;

            IList<string> commands = new List<string> { memberName };
            Command command = new MemberActivityVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}