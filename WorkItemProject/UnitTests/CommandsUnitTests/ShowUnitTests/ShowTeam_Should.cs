﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ShowTeam_Should : BaseTest
    {
        [TestMethod]
        public void ValidShowAllTeam_Should()
        {
            string team1 = "teamninjas";

            Team teamOne = new Team(team1);

            DataBase.AddTeam(teamOne);

            IList<string> commands = new List<string>();
            Command command = new ShowAllTeams(commands);

            string expected = teamOne.Name;
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InValidShowAllTeam_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new ShowAllTeams(commands);
            string expected = "There are no registered teams.";
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
