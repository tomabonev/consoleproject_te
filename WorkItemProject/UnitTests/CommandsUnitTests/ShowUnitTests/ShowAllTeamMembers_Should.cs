﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ShowAllTeamMembers_Should : BaseTest
    {
        [TestMethod]
        public void ValidShowTeamMembers_Should()
        {
            string memberName = "tomas";
            string teamName = "teamninjas";
            Member member = new Member(memberName);
            Team team = new Team(teamName);

            DataBase.AddMember(member);
            DataBase.AddTeam(team);
            team.Members.Add(member);
            

            IList<string> commands = new List<string> {teamName };
            Command command = new TeamMembersVisualizer(commands);
            string actual = command.Execute();
            string expected = string.Join(' ', team.Members.Select(a => a.ToString()));

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void ShowTeamMembersInvalidParameters_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new TeamMembersVisualizer(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowTeamMembersNameIsNULL_Should()
        {
            string memberName = null;

            IList<string> commands = new List<string> { memberName };
            Command command = new TeamMembersVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ShowTeamMembersNameIsEmpty_Should()
        {
            string memberName = string.Empty;

            IList<string> commands = new List<string> { memberName };
            Command command = new TeamMembersVisualizer(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidShowTeamMembers_Should()
        {
            string teamName = "teamninjas";
            Team team = new Team(teamName);

            DataBase.AddTeam(team);
            IList<string> commands = new List<string> { teamName };
            Command command = new TeamMembersVisualizer(commands);

            string actual = command.Execute();
            string expected = "Team doesn't have any members.";

            Assert.AreEqual(expected, actual);
        }
    }
}