﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class CreateMember_Should : BaseTest
    {
        [TestMethod]
        public void CreateValidMember_Should()
        {
            string memberName = "tomas";
            IList<string> commands = new List<string> { memberName };
            Command command = new MemberCreator(commands);

            Member member = new Member(memberName);

            string expected = $"Member with the name: {member.Name} was created.";
            string actual = command.Execute();

            var board = DataBase.GetMember(memberName);

            Assert.AreEqual(expected, actual);
            Assert.IsNotNull(board);
        }
        [TestMethod]
        public void MemberAlreadyExist_Should()
        {
            string memberName = "tomas";
            IList<string> commands = new List<string> { memberName };
            Command command = new MemberCreator(commands);

            Member member = new Member(memberName);
            DataBase.AddMember(member);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void MemberIsNULL_Should()
        {
            IList<string> commands = new List<string> { null };
            Command command = new MemberCreator(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void MemberIsEmpty_Should()
        {
            IList<string> commands = new List<string> { "" };
            Command command = new MemberCreator(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void CreateTeamInvalidParameters_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new MemberCreator(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
