﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class CreateStory_Should : BaseTest
    {
        [TestMethod]
        public void CreateValidStory_Should()
        {
            string title = "great story";
            string size = "Large";
            string priority = "High";
            string storyStatus = "Done";
            string boardName = "ninjasteam";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, size, priority, storyStatus, boardName, teamName, description };
            Command command = new StoryCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            string expected = $"Story id 1: {title} was successfully created.";
            string actual = command.Execute();
            var story = DataBase.GetStory(1);
            Assert.AreEqual(story.Title, title);
            Assert.AreEqual(story.Size, Enum.Parse<Size>(size, true));
            Assert.AreEqual(story.Priority, Enum.Parse<Priority>(priority, true));
            Assert.AreEqual(story.StoryStatus, Enum.Parse<StoryStatus>(storyStatus, true));
            Assert.AreEqual(story.Description, description);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void StoryEnumSizeIsInvalid_Should()
        {
            string title = "great story";
            string size = "Extra Large";
            string priority = "High";
            string storyStatus = "Done";
            string boardName = "ninjasteam";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, size, priority, storyStatus, boardName, teamName, description };
            Command command = new StoryCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void StoryEnumPriorityIsInvalid_Should()
        {
            string title = "great story";
            string size = "Large";
            string priority = "Extra High";
            string storyStatus = "Done";
            string boardName = "ninjasteam";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, size, priority, storyStatus, boardName, teamName, description };
            Command command = new StoryCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void StoryEnumStoryStatusIsInvalid_Should()
        {
            string title = "great story";
            string size = "Large";
            string priority = "High";
            string storyStatus = "Extra Done";
            string boardName = "ninjasteam";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, size, priority, storyStatus, boardName, teamName, description };
            Command command = new StoryCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void StoryTeamInBoardIsNULL_Should()
        {
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { boardName, "" };
            Command command = new StoryCreator(commands);

            Team team = new Team(teamName);
            team.AddBoard(new Board(boardName));

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void CreateStoryInvalidParameters_Should()
        {
            string title = "great story";
            string size = "Large";
            string priority = "High";
            string storyStatus = "Extra Done";
            string boardName = "ninjasteam";
            string teamName = "ninjasteamrules";

            IList<string> commands = new List<string> { title, size, priority, storyStatus, boardName, teamName };
            Command command = new StoryCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
