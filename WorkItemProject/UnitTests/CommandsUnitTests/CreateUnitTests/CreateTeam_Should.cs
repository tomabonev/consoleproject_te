﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class CreateTeam_Should : BaseTest
    {
        [TestMethod]
        public void CreateValidTeam_Should()
        {
            string teamName = "ninjasteam";
            IList<string> commands = new List<string> {teamName };
            Command command = new TeamCreator(commands);

            Team team = new Team(teamName);

            string expected = $"Team with the name: {team.Name} was created.";
            string actual = command.Execute();

            var board = DataBase.GetTeam(teamName);

            Assert.AreEqual(expected, actual);
            Assert.IsNotNull(board);
        }
        [TestMethod]
        public void TeamAlreadyExist_Should()
        {
            string teamName = "ninjasteam";
            IList<string> commands = new List<string> { teamName };
            Command command = new TeamCreator(commands);

            Team team = new Team(teamName);
            DataBase.AddTeam(team);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void TeamIsNULL_Should()
        {
            IList<string> commands = new List<string> { null };
            Command command = new TeamCreator(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void TeamIsEmpty_Should()
        {
            IList<string> commands = new List<string> { "" };
            Command command = new TeamCreator(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void CreateTeamInvalidParameters_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new TeamCreator(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
