﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class CreateBoard_Should : BaseTest
    {
        [TestMethod]
        public void CreateValidBoard_Should()
        {
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { boardName, teamName };
            Command command = new BoardCreator(commands);

            Team team = new Team(teamName);

            DataBase.AddTeam(team);
            string expected = $"Board with the name: {boardName} was created.";
            string actual = command.Execute();

            var board = DataBase.GetBoard(boardName, teamName);

            Assert.AreEqual(expected, actual);
            Assert.IsNotNull(board);
        }
        [TestMethod]
        public void BoardAlreadyExist_Should()
        {
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { boardName, teamName };
            Command command = new BoardCreator(commands);

            Team team = new Team(teamName);
            team.AddBoard(new Board(boardName));
            DataBase.AddTeam(team);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void BoardIsNULL_Should()
        {
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { "", teamName };
            Command command = new BoardCreator(commands);

            Team team = new Team(teamName);
            DataBase.AddTeam(team);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void TeamInBoardIsNULL_Should()
        {
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { boardName, "" };
            Command command = new BoardCreator(commands);

            Team team = new Team(teamName);
            team.AddBoard(new Board(boardName));

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void CreateBoardInvalidParameters_Should()
        {
            string boardName = "tickets";
            IList<string> commands = new List<string> { boardName};
            Command command = new BoardCreator(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
