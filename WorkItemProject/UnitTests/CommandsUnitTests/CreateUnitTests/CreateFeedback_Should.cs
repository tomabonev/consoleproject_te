﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class CreateFeedback_Should : BaseTest
    {
        [TestMethod]
        public void CreateValidFeedback_Should()
        {
            string title = "great feedback";
            string status = "Done";
            string rating = "5";
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, status, rating, boardName, teamName, description };
            Command command = new FeedCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);
           
            string expected = $"Feedback id 1: {title} was successfully created.";
            string actual = command.Execute();
            var feedback = DataBase.GetFeedBack(1);
            Assert.AreEqual(feedback.Title, title);
            Assert.AreEqual(feedback.FeedbackStatus, Enum.Parse<FeedbackStatus>(status,true));
            Assert.AreEqual(feedback.Rating, int.Parse(rating));
            Assert.AreEqual(feedback.Description, description);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FeedbackRatingIsInvalid_Should()
        {
            string title = "great feedback";
            string status = "Done";
            string rating = "six";
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, status, rating, boardName, teamName, description };
            Command command = new FeedCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void FeedbackEnumIsInvalid_Should()
        {
            string title = "great feedback";
            string status = "Hello";
            string rating = "5";
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            string description = "ninjasteamrules is completing the tasks on time";
            IList<string> commands = new List<string> { title, status, rating, boardName, teamName, description };
            Command command = new FeedCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void FeedbackTeamInBoardIsNULL_Should()
        {
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { boardName, "" };
            Command command = new FeedCreator(commands);

            Team team = new Team(teamName);
            team.AddBoard(new Board(boardName));

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void CreateFeedbackInvalidParameters_Should()
        {
            string title = "great feedback";
            string status = "Done";
            string rating = "5";
            string boardName = "tickets";
            string teamName = "ninjasteamrules";
            IList<string> commands = new List<string> { title, status, rating, boardName, teamName, };
            Command command = new FeedCreator(commands);

            Team team = new Team(teamName);
            Board board = new Board(boardName);
            DataBase.AddTeam(team);
            DataBase.AddBoard(board, teamName);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
