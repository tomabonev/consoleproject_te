﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeStoryPriority_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeStoryPriority_Should()
        {
            string storyId = "1";
            string priority = "High";
            var story = new Story("this is a story","it is a great story",default,default,default);
            IList<string> commands = new List<string> { storyId, priority };
            Command command = new StoryPriorityChanger(commands);

            DataBase.AddStory(story);

            string actual = command.Execute();
            string expected = $"Story item with title: {story.Title} was updated to a new priority: {story.Priority}.";
            Enum.TryParse<Priority>(priority, true, out Priority newStatus);

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(story.Priority, newStatus);
        }
        [TestMethod]
        public void ChangeStoryInvalidParameters_Should()
        {
            string storyId = "1";
            IList<string> commands = new List<string> { storyId };
            Command command = new StoryPriorityChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputStoryIdIsNULL_Should()
        {
            string storyId = "";
            string priority = "High";
            IList<string> commands = new List<string> { storyId, priority };
            Command command = new StoryPriorityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputStoryPriorityIsNULL_Should()
        {
            string storyId = "1";
            string priority = "";
            IList<string> commands = new List<string> { storyId, priority };
            Command command = new StoryPriorityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeStoryPriority_Should()
        {
            string storyId = "1";
            string priority = "Invalid priority";
            IList<string> commands = new List<string> { storyId, priority };
            Command command = new StoryPriorityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
