﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.FeedbackCommands;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeFeedbackRating_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeFeedbackRating_Should()
        {
            string feedId = "1";
            string rating = "5";
            var feedBack = new Feedback("createafeedback", "this is a great feedback", default, 3);
            IList<string> commands = new List<string> { feedId, rating };
            Command command = new FeedRatingChanger(commands);

            DataBase.AddFeedback(feedBack);

            string actual = command.Execute();
            string expected = $"Feedback item with title: {feedBack.Title} was updated to a new rating: {feedBack.Rating}.";

            Assert.AreEqual(actual, expected);
            Assert.AreEqual(feedBack.Rating, int.Parse(rating));
        }
        [TestMethod]
        public void ItemChangeFeedbackRatingNotFound_Should()
        {
            string feedId = "1";
            string rating = "5";
            var feedBack = new Feedback("createafeedback", "this is a great feedback", default, 3);
            IList<string> commands = new List<string> { feedId, rating };
            Command command = new FeedRatingChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void ChangeFeedbackRatingInvalidParameters_Should()
        {
            string feedId = "1";
            IList<string> commands = new List<string> { feedId };
            Command command = new FeedRatingChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputFeedbackRatingIdIsNULL_Should()
        {
            string feedId = "";
            string rating = "5";
            IList<string> commands = new List<string> { feedId, rating };
            Command command = new FeedRatingChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputFeedbackRatingIsNULL_Should()
        {
            string feedId = "1";
            string rating = "";
            IList<string> commands = new List<string> { feedId, rating };
            Command command = new FeedRatingChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeFeedbackRating_Should()
        {
            string feedId = "1";
            string rating = "Invalid rating";
            IList<string> commands = new List<string> { feedId, rating };
            Command command = new FeedRatingChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
