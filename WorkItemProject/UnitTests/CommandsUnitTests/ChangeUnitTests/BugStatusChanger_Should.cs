﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class BugStatusChanger_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeBugStatus_Should()
        {
            string bugId = "1";
            string status = "Fixed";
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Active, default, default, new List<string>());
            IList<string> commands = new List<string> { bugId, status };
            Command command = new BugStatusChanger(commands);

            DataBase.AddBug(bug);

            string actual = command.Execute();
            string expected = $"Bug item with title: {bug.Title} was updated to status: {bug.Status}";
            Enum.TryParse<BugStatus>(status, true, out BugStatus newStatus);

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(bug.Status, newStatus);
        }
        [TestMethod]
        public void ChangeBugStatusInvalidParameters_Should()
        {
            string bugId = "1";
            IList<string> commands = new List<string> { bugId };
            Command command = new BugStatusChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputBugStatusIdIsNULL_Should()
        {
            string bugId = "";
            string status = "Fixed";
            IList<string> commands = new List<string> { bugId, status };
            Command command = new BugStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputBugStatusIsNULL_Should()
        {
            string bugId = "1";
            string status = "";
            IList<string> commands = new List<string> { bugId, status };
            Command command = new BugStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeBugSeverity_Should()
        {
            string bugId = "1";
            string status = "Invalid severity";
            IList<string> commands = new List<string> { bugId, status };
            Command command = new BugStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
