﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeStoryStatus_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeStoryStatus_Should()
        {
            string storyId = "1";
            string status = "Done";
            var story = new Story("this is a story", "it is a great story", default, default, default);
            IList<string> commands = new List<string> { storyId, status };
            Command command = new StoryStatusChanger(commands);

            DataBase.AddStory(story);

            string actual = command.Execute();
            string expected = $"Story item with title: {story.Title} was updated to a new status: {story.StoryStatus}.";
            Enum.TryParse<StoryStatus>(status, true, out StoryStatus newStatus);

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(story.StoryStatus, newStatus);
        }
        [TestMethod]
        public void ChangeStoryStatusInvalidParameters_Should()
        {
            string storyId = "1";
            IList<string> commands = new List<string> { storyId };
            Command command = new StoryStatusChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputStoryIdIsNULL_Should()
        {
            string storyId = null;
            string status = "Done";
            IList<string> commands = new List<string> { storyId, status };
            Command command = new StoryStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputStoryStatusIsNULL_Should()
        {
            string storyId = "1";
            string status = null;
            IList<string> commands = new List<string> { storyId, status };
            Command command = new StoryStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeStoryStatus_Should()
        {
            string storyId = "1";
            string status = "Invalid priority";
            IList<string> commands = new List<string> { storyId, status };
            Command command = new StoryStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
