﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeStorySize_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeStorySize_Should()
        {
            string storyId = "1";
            string size = "Large";
            var story = new Story("this is a story", "it is a great story", default, default, default);
            IList<string> commands = new List<string> { storyId, size };
            Command command = new StorySizeChanger(commands);

            DataBase.AddStory(story);

            string actual = command.Execute();
            string expected = $"Story item with title: {story.Title} was updated to a new size: {story.Size}.";
            Enum.TryParse<Size>(size, true, out Size newStatus);

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(story.Size, newStatus);
        }
        [TestMethod]
        public void ChangeStorySizeInvalidParameters_Should()
        {
            string storyId = "1";
            IList<string> commands = new List<string> { storyId };
            Command command = new StorySizeChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputStoryIdIsNULL_Should()
        {
            string storyId = "";
            string size = "High";
            IList<string> commands = new List<string> { storyId, size };
            Command command = new StorySizeChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputStorySizeIsNULL_Should()
        {
            string storyId = "1";
            string size = "";
            IList<string> commands = new List<string> { storyId, size };
            Command command = new StorySizeChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeStorySize_Should()
        {
            string storyId = "1";
            string size = "Invalid priority";
            IList<string> commands = new List<string> { storyId, size };
            Command command = new StorySizeChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
