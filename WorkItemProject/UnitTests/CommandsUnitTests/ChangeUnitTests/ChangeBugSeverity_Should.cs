﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeBugSeverity_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeBugSeverity_Should()
        {
            string bugId = "1";
            string severity = "Critical";
            var bug = new Bug("brokencomputer", "computerisdamage", default, Severity.Critical, default, new List<string>());
            IList<string> commands = new List<string> { bugId, severity };
            Command command = new BugSeverityChanger(commands);

            DataBase.AddBug(bug);

            string actual = command.Execute();
            string expected = $"Bug item with title: {bug.Title} was updated to a new severity status: {bug.Severity}.";
            Enum.TryParse<Severity>(severity, true, out Severity newStatus);

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(bug.Severity, newStatus);
        }
        [TestMethod]
        public void ChangeBugSeverityInvalidParameters_Should()
        {
            string bugId = "1";
            IList<string> commands = new List<string> { bugId };
            Command command = new BugSeverityChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputBugSeverityIdIsNULL_Should()
        {
            string bugId = "";
            string severity = "Critical";
            IList<string> commands = new List<string> { bugId, severity };
            Command command = new BugSeverityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputBugSeverityIsNULL_Should()
        {
            string bugId = "1";
            string severity = "";
            IList<string> commands = new List<string> { bugId, severity };
            Command command = new BugSeverityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeBugSeverity_Should()
        {
            string bugId = "1";
            string severity = "Invalid severity";
            IList<string> commands = new List<string> { bugId, severity };
            Command command = new BugSeverityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
