﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeBugPriority_Should:BaseTest
    {
        [TestMethod]
        public void ValidChangeBugPriority_Should()
        {
            string bugId = "1";
            string priority = "High";
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, Priority.High, new List<string>());
            IList<string> commands = new List<string> { bugId, priority };
            Command command = new BugPriorityChanger(commands);

            DataBase.AddBug(bug);

            string actual = command.Execute();
            string expected = $"Bug item with title: {bug.Title} was updated to a new priority status: {bug.Priority}.";
            Enum.TryParse<Priority>(priority, true, out Priority newStatus);

            Assert.AreEqual(expected, actual);
            Assert.AreEqual(bug.Priority, newStatus);
        }
        [TestMethod]
        public void ChangeBugInvalidParameters_Should()
        {
            string bugId = "1";
            IList<string> commands = new List<string> { bugId };
            Command command = new BugPriorityChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputBugIdIsNULL_Should()
        {
            string bugId = "";
            string priority = "High";
            IList<string> commands = new List<string> { bugId, priority };
            Command command = new BugPriorityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputBugPriorityIsNULL_Should()
        {
            string bugId = "1";
            string priority = "";
            IList<string> commands = new List<string> { bugId, priority };
            Command command = new BugPriorityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeBugPriority_Should()
        {
            string bugId = "1";
            string priority = "Invalid priority";
            IList<string> commands = new List<string> { bugId, priority };
            Command command = new BugPriorityChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
