﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class ChangeFeedbackStatus_Should : BaseTest
    {
        [TestMethod]
        public void ValidChangeFeedbackStatus_Should()
        {
            string feedId = "1";
            string status = "Done";
            var feedBack = new Feedback("createafeedback", "this is a great feedback", default, 5);
            IList<string> commands = new List<string> { feedId, status };
            Command command = new FeedStatusChanger(commands);

            DataBase.AddFeedback(feedBack);

            string actual = command.Execute();
            string expected = $"Feedback item with title: {feedBack.Title} was updated to a new feed: {feedBack.FeedbackStatus}.";

            Enum.TryParse<FeedbackStatus>(status, true, out FeedbackStatus newStatus);

            Assert.AreEqual(actual, expected);
            Assert.AreEqual(feedBack.FeedbackStatus, newStatus);
        }
        [TestMethod]
        public void ChangeFeedbackStatusInvalidParameters_Should()
        {
            string feedId = "1";

            IList<string> commands = new List<string> { feedId };
            Command command = new FeedStatusChanger(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputFeedbackStatusIdIsNULL_Should()
        {
            string feedId = "";
            string status = "Done";
            IList<string> commands = new List<string> { feedId, status };
            Command command = new FeedStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InputFeedbackStatusIsNULL_Should()
        {
            string feedId = "1";
            string status = "";
            IList<string> commands = new List<string> { feedId, status };
            Command command = new FeedStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
        [TestMethod]
        public void InValidChangeFeedbackStatus_Should()
        {
            string feedId = "1";
            string status = "Invalid status";
            IList<string> commands = new List<string> { feedId, status };
            Command command = new FeedStatusChanger(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
