﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests
{
    [TestClass]
    public class AddCommentToItem_Should : BaseTest
    {
        [TestMethod]
        public void AddValidCommentToItem_Should()
        {
            string id = "1";
            string memberName = "tomas";
            string comment = "this is a comment for work item";
            IList<string> commands = new List<string> { id, memberName, comment };
            Command command = new WorkItemComment(commands);
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, Priority.High, new List<string>());
            Member member = new Member(memberName);
            Comment description = new Comment(member, comment);
            bug.Comments.Add(description);

            DataBase.AddBug(bug);
            DataBase.AddMember(member);

            string actual = command.Execute();
            string expected = $"Member: {memberName} has added a comment: {string.Join(" ", comment)} to the work item.";

            Assert.AreEqual(expected, actual);
            Assert.IsNotNull(memberName);
        }
        [TestMethod]
        public void AddCommentToWorkItemInvalidParameters_Should()
        {
            string id = "1";
            IList<string> commands = new List<string> { id };
            Command command = new WorkItemComment(commands);
            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
