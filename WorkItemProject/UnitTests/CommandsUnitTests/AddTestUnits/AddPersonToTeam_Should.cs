﻿using ManagementSystem.Core.Commands;
using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.AddTestUnits
{
    [TestClass]
    public class AddPersonToTeam_Should : BaseTest
    {
        [TestMethod]
        public void AddValidPersonToTeam_Should()
        {
            string memberName = "tomas";
            string teamName = "ninjasteamrules";

            List<string> commands = new List<string> { memberName, teamName };
            Command command = new TeamMemberAllocation(commands);

            Member member = new Member(memberName);
            Team team = new Team(teamName);

            DataBase.AddMember(member);
            DataBase.AddTeam(team);

            team.Members.Add(member);

            string actual = command.Execute();
            string expected = $"The member: {member.Name} has been added to a team: {team.Name} successfully.";

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InvalidAddPersonToTeamParameters_Should()
        {
            string memberName = "tomas";

            List<string> commands = new List<string> { memberName };
            Command command = new TeamMemberAllocation(commands);

            Assert.ThrowsException<ArgumentException>(() => command.Execute());
        }
    }
}
