﻿using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.ListWorkItemCommands;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ListStoryItems_Should : BaseTest
    {
        [TestMethod]
        public void ValidListStoryItems_Should()
        {
            Story story = new Story("this is a story", "it is a great story", default, default, default);

            DataBase.AddStory(story);

            IList<string> commands = new List<string>();
            Command command = new StoryItemsList(commands);

            string expected = story.ToString().Trim();
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InValidListStoryItems_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new StoryItemsList(commands);
            string expected = "There are no items in our Database.";
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
