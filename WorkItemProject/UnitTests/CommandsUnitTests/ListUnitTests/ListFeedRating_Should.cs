﻿using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.ListWorkItemCommands;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ListFeedRating_Should : BaseTest
    {
        [TestMethod]
        public void ValidListFeedRating_Should()
        {
            Feedback feedBack = new Feedback("createafeedback", "this is a great feedback", default, 5);
            DataBase.AddFeedback(feedBack);

            IList<string> commands = new List<string>();
            Command command = new FeedRatingList(commands);

            string expected = feedBack.ToString().Trim();
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InValidListFeedRating_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new FeedRatingList(commands);
            string expected = "There are no feed items in our Database.";
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
