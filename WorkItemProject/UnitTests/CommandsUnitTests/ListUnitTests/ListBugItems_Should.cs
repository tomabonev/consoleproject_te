﻿using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.ListWorkItemCommands;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ListBugItems_Should : BaseTest
    {
        [TestMethod]
        public void ValidListBugItems_Should()
        {
            Bug bug = new Bug("brokencomputer", "computerisdamage", default, default, Priority.High, new List<string>());

            DataBase.AddBug(bug);

            IList<string> commands = new List<string>();
            Command command = new BugItemsList(commands);

            string expected = bug.ToString().Trim();
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InValidListBugItems_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new BugItemsList(commands);
            string expected = "There are no items in our Database.";
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
