﻿using ManagementSystem.Core.Commands.Abstracts;
using ManagementSystem.Core.Commands.ListWorkItemCommands;
using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.CommandsUnitTests.ShowUnitTests
{
    [TestClass]
    public class ListFeedBackItems_Should : BaseTest
    {
        [TestMethod]
        public void ValidListFeedBackItems_Should()
        {
            Feedback feedBack = new Feedback("createafeedback", "this is a great feedback", default, 5);

            DataBase.AddFeedback(feedBack);

            IList<string> commands = new List<string>();
            Command command = new FeedItemsList(commands);

            string expected = feedBack.ToString().Trim();
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void InValidListFeedBackItems_Should()
        {
            IList<string> commands = new List<string>();
            Command command = new FeedItemsList(commands);
            string expected = "There are no items in our Database.";
            string actual = command.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
