﻿namespace UnitTests.BaseUnitTest
{
    using System;
    using System.Reflection;
    using ManagementSystem.Core.Contracts;
    using ManagementSystem.Core.Database;
    using ManagementSystem.Models.Abstract;
    using ManagementSystem.Models.Classes;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BaseTest
    {
        protected IDatabase DataBase;
        [TestInitialize]
        public void ResetState()
        {
            var instanceField = typeof(Database).GetField("instance", BindingFlags.NonPublic | BindingFlags.Static);
            var newSingleton = Activator.CreateInstance(typeof(Database), true);
            instanceField.SetValue(newSingleton, newSingleton);
            DataBase = newSingleton as IDatabase;

            var idField = typeof(WorkItem).GetField("id", BindingFlags.NonPublic | BindingFlags.Static);
            var newId = 1;
            var workItem = new Feedback("Valid Title", "This is magic", default, 5);
            idField.SetValue(workItem, newId);
        }
    }
}
