﻿using ManagementSystem.Core.Factories;
using ManagementSystem.Enums;
using ManagementSystem.Models.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.FactoryUnitTest
{
    [TestClass]
    public class FactoryShould:BaseTest
    {
        [TestMethod]
        public void ValidCreateBug_Should()
        {
            string title = "ninjasteam";
            string description = "ninjasteam is the best";
            List<string> stepsToReproduce = new List<string> { "step1","step2"};

            IBug createBug = Factory.Instance.CreateBug(title, description, BugStatus.Fixed, Severity.Critical, Priority.High, stepsToReproduce);
            Assert.IsInstanceOfType(createBug, typeof(IBug));
        }
        [TestMethod]
        public void ValidCreateFeedBack_Should()
        {
            string title = "ninjasteam";
            string description = "ninjasteam is the best";
            int rating = 5;

            IFeedBack createFeedback = Factory.Instance.CreateFeedback(title, description, FeedbackStatus.Done,rating);
            Assert.IsInstanceOfType(createFeedback, typeof(IFeedBack));
        }
        [TestMethod]
        public void ValidStory_Should()
        {
            string title = "ninjasteam";
            string description = "ninjasteam is the best";

            IStory createStory = Factory.Instance.CreateStory(title, description, Size.Large,Priority.High,StoryStatus.Done);
            Assert.IsInstanceOfType(createStory, typeof(IStory));
        }
        [TestMethod]
        public void ValidCreateBoard_Should()
        {
            string name = "ninjasteam";

            IBoard createBoard = Factory.Instance.CreateBoard(name);
            Assert.IsInstanceOfType(createBoard, typeof(IBoard));
        }
        [TestMethod]
        public void ValidCreateMember_Should()
        {
            string name = "tomas";

            IMember createMember = Factory.Instance.CreateMember(name);
            Assert.IsInstanceOfType(createMember, typeof(IMember));
        }
        [TestMethod]
        public void ValidCreateTeam_Should()
        {
            string team = "tomasteam";

            ITeam createTeam = Factory.Instance.CreateTeam(team);
            Assert.IsInstanceOfType(createTeam, typeof(ITeam));
        }
    }
}
