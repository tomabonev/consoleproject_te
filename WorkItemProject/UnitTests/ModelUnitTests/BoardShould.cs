﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Abstract;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class BoardShould : BaseTest
    {
        [TestMethod]
        public void TestBoardName()
        {
            var board = new Board("ninjasteam");

            string expectedName = "ninjasteam";
            string actualName = board.Name;

            Assert.AreEqual(expectedName, actualName);
        }
        [TestMethod]
        public void BoardNameMinLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Board("ni"));
        }
        [TestMethod]
        public void BoardNameMaxLength()
        {
            var longName = new string('a', 11);
            Assert.ThrowsException<ArgumentException>(() => new Board(longName));
        }
        [TestMethod]
        public void TestBoardNameIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Board(null));
        }
        [TestMethod]
        public void TestAddWorkItem()
        {
            var board = new Board("validBoard");
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());

            board.Add(bug);
            Assert.AreEqual(board.WorkItems[0], bug);
        }
        [TestMethod]
        public void ToStringBoard_Should()
        {
            Board board = new Board("boarddata");

            string NL = Environment.NewLine;
            string actual = $"Name: {board.Name}{NL}" +
                            $"Work Items: {string.Join(" ", board.WorkItems)}{NL}" +
                            $"History: {string.Join(" ", board.History)}{NL}";

            Assert.AreEqual(board.ToString(), actual);
        }
    }
}
