﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class StoryShould : BaseTest
    {
        [TestMethod]
        public void TestStorySize()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);

            Size expectedSize = Size.Large;
            Size actualSize = story.Size;

            Assert.AreEqual(expectedSize, actualSize);
        }
        [TestMethod]
        public void TestStoryPriority()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);

            Priority expectedPriority = Priority.High;
            Priority actualPriority = story.Priority;

            Assert.AreEqual(expectedPriority, actualPriority);
        }
        [TestMethod]
        public void TestStoryStatus()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);

            StoryStatus expectedStatus = StoryStatus.Done;
            StoryStatus actualStatus = story.StoryStatus;

            Assert.AreEqual(expectedStatus, actualStatus);
        }
        [TestMethod]
        public void ToStringStory_Should()
        {
            Story story = new Story("whatastory", "this was epic", Size.Large, Priority.High, StoryStatus.Done);

            string NL = Environment.NewLine;
            string actual =$"Type: Story{NL}" +
                            $"Work Item ID: {story.Id}{NL}" +
                            $"Title: {story.Title}{NL}" +
                            $"Description: {story.Description}{NL}" +
                            $"Comment from the author: {string.Join(" ", story.Comments)}{NL}" +
                            $"---Story Info---{NL}" +
                            $"Story Size: {story.Size}{NL}" +
                            $"Story Priority: {story.Priority}{NL}" +
                            $"Story Status: {story.StoryStatus}{NL}" +
                            $"Assignee: {story.GetAssignee()}{NL}";

            Assert.AreEqual(story.ToString(), actual);
        }
        [TestMethod]
        public void GetAssigneeValidStory_Should()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);
            Member member = new Member("tomas");
            story.Assignee = member;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"tomas");
            string actual = sb.ToString().Trim();

            Assert.AreEqual(story.GetAssignee(), actual);
        }
        [TestMethod]
        public void GetAssigneeInvalidStory_Should()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Assignee: none");
            string actual = sb.ToString().Trim();

            Assert.AreEqual(story.GetAssignee(), actual);
        }
    }
}
