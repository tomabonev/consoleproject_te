﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class TeamShould : BaseTest
    {
        [TestMethod]
        public void TestTeamName()
        {
            var team = new Team("ninjasteamrules");

            string expectedName = "ninjasteamrules";
            string actualName = team.Name;

            Assert.AreEqual(expectedName, actualName);
        }
        [TestMethod]
        public void TestTeamNameMinLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Team("ni"));
        }
        [TestMethod]
        public void TestTeamNameMaxLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Team("ninjasteamrulesbug"));
        }
        [TestMethod]
        public void TestTeamNameIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Team(null));
        }
        [TestMethod]
        public void TestAddBoard()
        {
            var team = new Team("ninjasteamrules");
            var board = new Board("boardData");

            team.AddBoard(board);
            Assert.AreEqual(team.Boards[0], board);
        }
        [TestMethod]
        public void ToStringTeam_Should()
        {
            Team team = new Team("ninjasteam");

            string NL = Environment.NewLine;
            string actual = $"Name: {team.Name}----{NL}" +
                            $"Members: {team.Members}----{NL}" +
                            $"Boards: {team.Boards}----{NL}" +
                            $"History: {string.Join(" ", team.History)}----{NL}";

            Assert.AreEqual(team.ToString(), actual);
        }
    }
}
