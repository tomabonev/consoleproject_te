﻿using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class CommentShould : BaseTest
    {
        [TestMethod]
        public void TestCommentConstructor()
        {
            var member = new Member("tomas");
            var comment = new Comment(member, "asenkata is a beast");

            string expectedDescription = "asenkata is a beast";
            string actualDescription = comment.Description;

            Assert.AreEqual(expectedDescription, actualDescription);
        }
        [TestMethod]
        public void TestMemberConstructor()
        {
            var member = new Member("tomas");

            string expectedMember = "tomas";
            string actualMember = member.Name;

            Assert.AreEqual(expectedMember, actualMember);
        }
        [TestMethod]
        public void TestCommentDescriptionMinLength()
        {
            var member = new Member("tomas");
            Assert.ThrowsException<ArgumentException>(() => new Comment(member, "asenkata"));
        }
        [TestMethod]
        public void TestCommentDescriptionMaxLength()
        {
            var longComment = new string('t',51);
            var member = new Member("tomas");
            Assert.ThrowsException<ArgumentException>(() => new Comment(member, longComment));
        }
        [TestMethod]
        public void TestCommentNull()
        {
            var member = new Member("tomas");
            string nullComment = null;
            Assert.ThrowsException<ArgumentException>(() => new Comment(member, nullComment));
        }
        [TestMethod]
        public void ToStringComment_Should()
        {
            Member member = new Member("tomas");
            Comment comment = new Comment(member, "this is a comment description");

            string NL = Environment.NewLine;
            string actual=  $"{comment.Author.Name}{NL}" +
                            $"Has added the following description: {comment.Description}{NL}" +
                            $"It was added at: {comment.Date}{NL}";

            Assert.AreEqual(comment.ToString(), actual);
        }
    }
}
