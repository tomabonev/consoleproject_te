﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class FeedbackShould : BaseTest
    {
        [TestMethod]
        public void TestFeedbackStatus()
        {
            var feedback = new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done, 5);

            FeedbackStatus expectedStatus = FeedbackStatus.Done;
            FeedbackStatus actualStatus = feedback.FeedbackStatus;

            Assert.AreEqual(expectedStatus, actualStatus);
        }
        [TestMethod]
        public void TestFeedbackRating()
        {
            var feedback = new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done, 5);

            int expectedRating = 5;
            int actualRating = feedback.Rating;

            Assert.AreEqual(expectedRating, actualRating);
        }
        [TestMethod]
        public void TestRatingMinValue()
        {
            Assert.ThrowsException<ArgumentException>(() => new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done, -5));
        }
        [TestMethod]
        public void TestRatingMaxValue()
        {
            Assert.ThrowsException<ArgumentException>(() => new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done, 10));
        }
        [TestMethod]
        public void ToStringFeedback_Should()
        {
            Feedback feedback = new Feedback("excelentFeed", "what a great feedback", FeedbackStatus.Done, 5);

            string NL = Environment.NewLine;
            string actual = $"Type: Feedback{NL}" +
                            $"Work Item ID: {feedback.Id}{NL}" +
                            $"Title: {feedback.Title}{NL}" +
                            $"Description: {feedback.Description}{NL}" +
                            $"---Feedback Info---{NL}" +
                            $"Feedback status: {feedback.FeedbackStatus}{NL}" +
                            $"Feedback Rating: {feedback.Rating}{NL}";

            Assert.AreEqual(feedback.ToString(), actual);
        }

    }
}

