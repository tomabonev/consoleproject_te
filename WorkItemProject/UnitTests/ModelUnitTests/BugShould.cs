﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using UnitTests.BaseUnitTest;

namespace UnitTests.UnitTests
{
    [TestClass]
    public class BugShould : BaseTest
    {
        [TestMethod]
        public void TestBugStatus()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());

            BugStatus expectedStatus = BugStatus.Fixed;
            BugStatus actualStatus = bug.Status;

            Assert.AreEqual(expectedStatus, actualStatus);
        }
        [TestMethod]
        public void TestBugSeverity()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());

            Severity expectedSeverity = Severity.Major;
            Severity actualSeverity = bug.Severity;

            Assert.AreEqual(expectedSeverity, actualSeverity);
        }
        [TestMethod]
        public void TestBugPriority()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());

            Priority expectedPriority = Priority.High;
            Priority actualPriority = bug.Priority;

            Assert.AreEqual(expectedPriority, actualPriority);
        }
        [TestMethod]
        public void TestBugListIsNotNull()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            Assert.IsNotNull(bug.StepsToReproduce);
        }
        [TestMethod]
        public void ToStringBug_Should()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            string NL = Environment.NewLine;
            string actual = $"Type: Bug{NL}" +
                             $"Work Item ID: {bug.Id}{NL}" +
                             $"Title: {bug.Title}{NL}" +
                             $"Description: {bug.Description}{NL}" +
                             $"Comment from the author: {string.Join(" ", bug.Comments)}{NL}" +
                             $"---Bug Info---{NL}" +
                             $"Bug Status: {bug.Status}{NL}" +
                             $"Bug Severity: {bug.Severity}{NL}" +
                             $"Bug Priority: {bug.Priority}{NL}" +
                             $"------{NL}" +
                             $"Assignee: {bug.GetAssignee()}{NL}" +
                             $"------{NL}" +
                             $"StepsToReproduce: {NL}" +
                             $"{bug.GetStepToProduce()}{NL}" +
                             $"------{NL}";

            Assert.AreEqual(bug.ToString(), actual);
        }
        [TestMethod]
        public void GetAssigneeValidBug_Should()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());
            Member member = new Member("tomas");
            bug.Assignee = member;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"tomas");
            string actual = sb.ToString().Trim();

            Assert.AreEqual(bug.GetAssignee(), actual);
        }
        [TestMethod]
        public void GetAssigneeInvalidBug_Should()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"none");
            string actual = sb.ToString().Trim();

            Assert.AreEqual(bug.GetAssignee(), actual);
        }
        [TestMethod]
        public void GetStepToReproduceValidBug_Should()
        {
            List<string> steps = new List<string> { "step1" };
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, steps);

            string NL = Environment.NewLine;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"{string.Join(NL, bug.StepsToReproduce)}");
            string actual = sb.ToString().Trim();

            Assert.AreEqual(bug.GetStepToProduce(), actual);
        }
        [TestMethod]
        public void GetStepToReproduceInValidBug_Should()
        {
            List<string> steps = new List<string>();
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, steps);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("none");
            string actual = sb.ToString().Trim();

            Assert.AreEqual(bug.GetStepToProduce(), actual);
        }
    }
}
