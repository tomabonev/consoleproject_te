﻿using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class EventHistoryShould : BaseTest
    {
        [TestMethod]
        public void TestEventHistory()
        {
            var history = new EventHistory("ninjasteam is the best");

            string expectedName = "ninjasteam is the best";
            string actualName = history.Description;

            Assert.AreEqual(expectedName, actualName);
        }
        [TestMethod]
        public void ToStringEventHistory_Should()
        {
            //Arrange
            EventHistory history = new EventHistory("event history description");

            //Act
            string NL = Environment.NewLine;
            string actual=  $"{history.Description}{NL}" +
                            $"Added on: {history.DateTime}{NL}";

            //Assert
            Assert.AreEqual(history.ToString(), actual);
        }
    }
}
