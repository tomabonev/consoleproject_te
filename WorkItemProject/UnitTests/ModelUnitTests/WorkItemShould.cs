﻿using ManagementSystem.Models.Abstract;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UnitTests.BaseUnitTest;

namespace UnitTests.UnitTests
{
    [TestClass]
    public class WorkItemShould : BaseTest
    {
        [TestMethod]
        public void TestWorkItemConstructor()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            string expectedTitle = "brokencomputer";
            string actualTitle = bug.Title;
            int actualId = bug.Id;
            int expectedlId = 1;

            Assert.AreEqual(expectedTitle, actualTitle);
            Assert.AreEqual(actualId, expectedlId);
        }
        [TestMethod]
        public void TestTitleMinLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Bug("broken", "computerisdamage", default, default, default, new List<string>()));
        }
        [TestMethod]
        public void TestTitleMaxLength()
        {
            string longTitle = "i am coming back from the sea and right away i am sitting to write test methods.";
            Assert.ThrowsException<ArgumentException>(() => new Bug(longTitle, "computerisdamage", default, default, default, new List<string>()));
        }
        [TestMethod]
        public void TestTitleNull()
        {
            string nullTitle = null;
            Assert.ThrowsException<ArgumentException>(() => new Bug(nullTitle, "computerisdamage", default, default, default, new List<string>()));
        }
        [TestMethod]
        public void TestDescriptionMinLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Bug("brokencomputer", "computer", default, default, default, new List<string>()));
        }
        [TestMethod]
        public void TestDescriptionMaxLength()
        {
            string longTitle = new string('a', 501);

            Assert.ThrowsException<ArgumentException>(() => new Bug("brokencomputer", longTitle, default, default, default, new List<string>()));
        }
        [TestMethod]
        public void TestNullDescription()
        {
            string nullTitle = null;
            Assert.ThrowsException<ArgumentException>(() => new Bug("brokencomputer", nullTitle, default, default, default, new List<string>()));
        }
        [TestMethod]
        public void TestWorkItemCommentListNull()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            Assert.IsNotNull(bug.Comments);         
        }
        [TestMethod]
        public void TestWorkItemHistoryListNull()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            Assert.IsNotNull(bug.History);
        }
        [TestMethod]
        public void ToStringItem_Should()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", default, default, default, new List<string>());

            string NL = Environment.NewLine;
            string actual = $"Type: Bug{NL}" +
                             $"Work Item ID: {bug.Id}{NL}" +
                             $"Title: {bug.Title}{NL}" +
                             $"Description: {bug.Description}{NL}" +
                             $"Comment from the author: {string.Join(" ", bug.Comments)}{NL}" +
                             $"---Bug Info---{NL}" +
                             $"Bug Status: {bug.Status}{NL}" +
                             $"Bug Severity: {bug.Severity}{NL}" +
                             $"Bug Priority: {bug.Priority}{NL}" +
                             $"------{NL}" +
                             $"Assignee: {bug.GetAssignee()}{NL}" +
                             $"------{NL}" +
                             $"StepsToReproduce: {NL}" +
                             $"{bug.GetStepToProduce()}{NL}" +
                             $"------{NL}";

            Assert.AreEqual(bug.ToString(), actual);
        }
    }
}
