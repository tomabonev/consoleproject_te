﻿using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using UnitTests.BaseUnitTest;

namespace UnitTests.ModelUnitTests
{
    [TestClass]
    public class MemberShould : BaseTest
    {
        [TestMethod]
        public void TestMemberName()
        {
            var member = new Member("ninjasteam");

            string expectedName = "ninjasteam";
            string actualName = member.Name;

            Assert.AreEqual(expectedName, actualName);
        }
        [TestMethod]
        public void MemberNameMinLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Member("ninj"));
        }
        [TestMethod]
        public void MemberNameMaxLength()
        {
            Assert.ThrowsException<ArgumentException>(() => new Member("ninjas team is the best one"));
        }
        [TestMethod]
        public void TestMemberNameIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Member(null));
        }
        [TestMethod]
        public void ToStringMember_Should()
        {
            Member member = new Member("tomas");

            string NL = Environment.NewLine;
            string actual = $"------------------------{NL}" +
                            $"Member Name: {member.Name}{NL}" +
                            $"------------------------{NL}" +
                            $"{string.Join(" ", member.WorkItems.Select(items => items.ToString()))}{NL}" +
                            $"------{NL}" +
                            $"History:{NL}" +
                            $"------{NL}" +
                            $"{string.Join(" ", member.History)}{NL}";

            Assert.AreEqual(member.ToString(), actual);
        }
    }
}
