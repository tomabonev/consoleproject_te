﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using UnitTests.BaseUnitTest;

namespace UnitTests.DataBaseUnitTests
{
    [TestClass]
    public class DataBaseMethodsShould : BaseTest
    {
        [TestMethod]
        public void AddMemberValidMethod()
        {
            var member = new Member("Tomas");

            DataBase.AddMember(member);

            var memberDatabase = DataBase.Members.FirstOrDefault(name => name.Name == "Tomas");
            Assert.AreEqual(memberDatabase, member);
        }
        [TestMethod]
        public void AddMemberInvalidMethod()
        {
            var member = new Member("Tomas");

            DataBase.AddMember(member);

            Assert.ThrowsException<ArgumentException>(() => DataBase.AddMember(member));
        }
        [TestMethod]
        public void AddMemberNullMethod()
        {
            Assert.ThrowsException<ArgumentException>(() => DataBase.AddMember(null));
        }
        [TestMethod]
        public void GetMemberValidMethod()
        {
            var member = new Member("Tomas");
            DataBase.AddMember(member);
            var newmember = DataBase.GetMember("Tomas");

            Assert.AreEqual(member, newmember);
        }
        [TestMethod]
        public void GetTeamValidMethod()
        {
            var team = new Team("ninjasteamrules");
            DataBase.AddTeam(team);
            var newTeam = DataBase.GetTeam("ninjasteamrules");

            Assert.AreEqual(team, newTeam);
        }
        [TestMethod]
        public void AddTeamValidMethod()
        {
            var team = new Team("ninjasteamrules");

            DataBase.AddTeam(team);

            var teamDatabase = DataBase.Teams.FirstOrDefault(name => name.Name == "ninjasteamrules");
            Assert.AreEqual(teamDatabase, team);
        }
        [TestMethod]
        public void AddTeamInvalidMethod()
        {
            var team = new Team("Tomas");

            DataBase.AddTeam(team);

            Assert.ThrowsException<ArgumentException>(() => DataBase.AddTeam(team));
        }
        [TestMethod]
        public void AddTeamNullMethod()
        {
            Assert.ThrowsException<ArgumentException>(() => DataBase.AddTeam(null));
        }
        [TestMethod]
        public void AddBoardToTeamValidMethod()
        {
            var team = new Team("ninjasrulesteam");
            var board = new Board("Serengeti");

            DataBase.AddTeam(team);
            DataBase.AddBoard(board,team.Name);

            Assert.AreEqual(team.Boards[0],board);
        }
        [TestMethod]
        public void AddBoardNullToTeamMethod()
        {
            var team = new Team("ninjasrulesteam");
            DataBase.AddTeam(team);
            Assert.ThrowsException<ArgumentException>(() => DataBase.AddBoard(null,team.Name));
        }
        [TestMethod]
        public void AddBoardToTeamNullMethod()
        {
            var board = new Board("Serengeti");
            Assert.ThrowsException<ArgumentException>(() => DataBase.AddBoard(board, null));
        }
        [TestMethod]
        public void GetBoardToTeamNullMethod()
        {
            var team = new Team("ninjasteamrules");
            DataBase.AddTeam(team);
            var board = new Board("Serengeti");
            DataBase.AddBoard(board, team.Name);

            Assert.ThrowsException<ArgumentException>(() => DataBase.GetBoard("Serengeti", null));
        }
        [TestMethod]
        public void GetBoardToTeamValidMethod()
        {
            var team = new Team("ninjasteamrules");
            DataBase.AddTeam(team);
            var board = new Board("Serengeti");
            DataBase.AddBoard(board, team.Name);

            var newBoard = DataBase.GetBoard("Serengeti", "ninjasteamrules");

            Assert.AreEqual(team.Boards[0], newBoard);
        }

        [TestMethod]
        public void AddBugValidMethod()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());
            DataBase.AddBug(bug);
            var newBug = DataBase.GetBug(1);

            Assert.AreEqual(bug, newBug);
        }
        [TestMethod]
        public void AddBugInValidMethod()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());
            DataBase.AddBug(bug);

            Assert.ThrowsException<ArgumentException>(() => DataBase.AddBug(bug));
        }
        [TestMethod]
        public void AddStoryValidMethod()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High,StoryStatus.Done);
            DataBase.AddStory(story);
            var newStory = DataBase.GetStory(1);

            Assert.AreEqual(story, newStory);
        }
        [TestMethod]
        public void AddStoryInValidMethod()
        {
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);
            DataBase.AddStory(story);

            Assert.ThrowsException<ArgumentException>(() => DataBase.AddStory(story));
        }
        [TestMethod]
        public void AddFeedBackValidMethod()
        {
            var feedback = new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done,5);
            DataBase.AddFeedback(feedback);
            var newFeedback = DataBase.GetFeedBack(1);

            Assert.AreEqual(feedback, newFeedback);
        }
        [TestMethod]
        public void AddFeedBackInValidMethod()
        {
            var feedback = new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done, 5);
            DataBase.AddFeedback(feedback);

            Assert.ThrowsException<ArgumentException>(() => DataBase.AddFeedback(feedback));
        }
        [TestMethod]
        public void GetWorkItemValidMethod()
        {
            var bug = new Bug("brokencomputer", "computerisdamage", BugStatus.Fixed, Severity.Major, Priority.High, new List<string>());
            var story = new Story("brokencomputer", "computerisdamage", Size.Large, Priority.High, StoryStatus.Done);
            var feedback = new Feedback("brokencomputer", "computerisdamage", FeedbackStatus.Done, 5);
            DataBase.AddBug(bug);
            DataBase.AddStory(story);
            DataBase.AddFeedback(feedback);

            Assert.AreSame(DataBase.GetItem(1),bug);
            Assert.AreSame(DataBase.GetItem(2),story);
            Assert.AreSame(DataBase.GetItem(3),feedback);
        }
        [TestMethod]
        public void GetWorkItemInValidMethod()
        {
            Assert.IsNull(DataBase.GetItem(1));
        }
    }
}
