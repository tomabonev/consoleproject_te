﻿namespace ManagementSystem.Enums
{
    public enum Severity
    {
        Minor = 0,
        Major = 1,
        Critical = 2
    }
}
