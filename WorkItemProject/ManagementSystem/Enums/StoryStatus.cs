﻿namespace ManagementSystem.Enums
{
    public enum StoryStatus
    {
        NotDone = 0,
        InProgress = 1,
        Done = 2
    }
}
