﻿namespace ManagementSystem.Enums
{
    public enum FeedbackStatus
    {
        New = 0,
        Unscheduled = 1,
        Scheduled = 2,
        Done = 3          
    }
}
