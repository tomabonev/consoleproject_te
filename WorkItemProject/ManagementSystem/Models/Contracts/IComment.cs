﻿namespace ManagementSystem.Models.Contracts
{
    using System;

    /// <summary>
    /// Defines the functionality of a member comment. 
    /// </summary>
    public interface IComment
    {
        IMember Author { get; }

        string Description { get; }

        DateTime Date { get; }

        string ToString();
    }
}
