﻿namespace ManagementSystem.Models.Contracts
{
    using System.Collections.Generic;
    using ManagementSystem.Enums;

    /// <summary>
    /// Defines the functionality of a bug item. 
    /// </summary>
    public interface IBug : IWorkItem, IPriority, IAssignee
    {
        BugStatus Status { get; set; }

        Severity Severity { get; set; }

        IList<string> StepsToReproduce { get; set; }

        string GetAssignee();

        string GetStepToProduce();

    }
}
