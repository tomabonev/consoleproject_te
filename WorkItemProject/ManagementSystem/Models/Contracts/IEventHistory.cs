﻿namespace ManagementSystem.Models.Contracts
{
    using System;

    /// <summary>
    /// Defines the functionality of the event history. 
    /// </summary>
    public interface IEventHistory
    {
        string Description { get; set; }

        DateTime DateTime { get; }

        string ToString();
    }
}