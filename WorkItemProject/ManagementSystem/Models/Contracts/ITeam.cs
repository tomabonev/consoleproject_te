﻿namespace ManagementSystem.Models.Contracts
{
    using System.Collections.Generic;
    using ManagementSystem.Models.Classes;

    /// <summary>
    /// Defines the characteristic of a team. 
    /// </summary>
    public interface ITeam
    {
        string Name { get; }

        IList<IMember> Members { get; }

        IList<IBoard> Boards { get; }

        IList<EventHistory> History { get; }

        void AddBoard(IBoard board);

        string ToString();
    }
}