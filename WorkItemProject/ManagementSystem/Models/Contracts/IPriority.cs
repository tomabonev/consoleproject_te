﻿namespace ManagementSystem.Models.Contracts
{
    using ManagementSystem.Enums;

    /// <summary>
    /// Defines the functionality of the priority class. 
    /// </summary>
    public interface IPriority
    {
        Priority Priority { get; set; }
    }
}