﻿namespace ManagementSystem.Models.Contracts
{
    using System.Collections.Generic;
    using ManagementSystem.Models.Classes;

    /// <summary>
    /// Defines the characteristic of a member. 
    /// </summary>
    public interface IMember
    {
        string Name { get; }

        IList<IWorkItem> WorkItems { get; }

        IList<EventHistory> History { get; }

        string ToString();
    }
}