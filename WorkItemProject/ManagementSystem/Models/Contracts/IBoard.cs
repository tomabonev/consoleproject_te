﻿namespace ManagementSystem.Models.Contracts
{
    using ManagementSystem.Models.Classes;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the functionality of a board. 
    /// </summary>
    public interface IBoard
    {
        string Name { get; }

        IReadOnlyList<IWorkItem> WorkItems { get; }

        IList<EventHistory> History { get; }

        void Add(IWorkItem item);

        string ToString();
    }
}
