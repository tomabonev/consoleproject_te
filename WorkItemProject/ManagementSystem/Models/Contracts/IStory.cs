﻿namespace ManagementSystem.Models.Contracts
{
    using ManagementSystem.Enums;


    /// <summary>
    /// Defines the functionality of a story item. 
    /// </summary>
    public interface IStory : IWorkItem, IPriority, IAssignee
    {
        Size Size { get; set; }

        StoryStatus StoryStatus { get; set; }

        string GetAssignee();
    }
}