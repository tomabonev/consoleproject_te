﻿namespace ManagementSystem.Models.Contracts
{
    using ManagementSystem.Enums;

    /// <summary>
    /// Defines the functionality of the feedback item. 
    /// </summary>
    public interface IFeedBack : IWorkItem
    {
        int Rating { get; set; }

        FeedbackStatus FeedbackStatus { get; set; }
    }
}