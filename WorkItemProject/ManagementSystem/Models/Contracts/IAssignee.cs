﻿namespace ManagementSystem.Models.Contracts
{
    public interface IAssignee
    {
        IMember Assignee { get; set; }
    }
}
