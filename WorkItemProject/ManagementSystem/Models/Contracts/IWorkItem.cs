﻿namespace ManagementSystem.Models.Contracts
{
    using System.Collections.Generic;
    using ManagementSystem.Models.Classes;


    /// <summary>
    /// Defines the functionality of a work item. 
    /// </summary>
    public interface IWorkItem
    {
        int Id { get; }

        string Title { get; set; }

        string Description { get; set; }

        IList<Comment> Comments { get; }

        IList<EventHistory> History { get; }

        string ToString();
    }
}