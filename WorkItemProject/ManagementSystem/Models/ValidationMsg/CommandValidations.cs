﻿namespace ManagementSystem.Models.ValidationMsg
{
    /// <summary>
    /// Static validation commands to be implemented in the exception messages
    /// </summary>
    public static class CommandValidations
    {
        public const string EmptyValue = "Value cannot be null or empty.";
        public const string NullItemInDatabase = "{0} cannot be null in our Database.";
        public const string ValueLength = "There needs to be {0} parameters submitted.";
        public const string EmptyItemInDatabase = "There is no {0} in the Database.";
        public const string EmptyMemberInDatabase = "There is no such {0} in the Database.";
        public const string BoardExists = "Board already exist in Team!";
        public const string InvalidEnumStatus = "There is no such {0} status.";
        public const string InvalidFeedbackRating = "The rating is out of scope.";
        public const string ExistingMember = "This member name allready exist in our Database.";
    }
}

