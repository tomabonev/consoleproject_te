﻿namespace ManagementSystem.Models.ValidationMsg
{
    /// <summary>
    /// Static validation commands to be implemented in the exception messages
    /// </summary>
    public static class Validation
    {
        public static string InvalidTitle = "Title name cannot be NULL or empty.";
        public static string InvalidDescprition = "Description name cannot be NULL or empty.";
        public static string InvalidTeamName = "Team name cannot be empty.";
        public static string InvalidTeamNameLength = "Team name must be between 3 and 15 symbols.";
        public static string InvalidTitleLength = "Title must be between 10 and 50 symbols.";
        public static string InvalidCommentLength = "Description must be between 10 and 50 symbols.";
        public static string InvalidBoardName = "Name must be between 5 and 10 symbols.";
        public static string InvalidMemberName = "Name must be between 5 and 15 symbols.";
        public static string InvalidDescriptionLength = "Description must be between 10 and 500 symbols.";
        public static string InvalidDescription = "Description name cannot be NULL or empty.";
        public static string InvalidListLegth = "List cannot be out of range.";
        public static string InvalidRating = "Rating cannot be negative."; 
        public static string BoardExist = "Board already exist!"; 
    }
}