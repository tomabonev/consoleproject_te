﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using System.Text;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Abstract;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Story class that inherits from abstract class workitems.
    /// </summary>
    public class Story : WorkItem, IStory
    {
        public Story(string title,
                     string description,
                     Size size,
                     Priority priority,
                     StoryStatus storyStatus)
                     : base(title, description)
        {
            this.Size = size;
            this.Priority = priority;
            this.StoryStatus = storyStatus;
        }

        public Size Size { get; set; }

        public Priority Priority { get; set; }

        public StoryStatus StoryStatus { get; set; }

        public IMember Assignee { get; set; }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            return $"Type: Story{NL}" +
            base.ToString() +
            $"Comment from the author: {string.Join(" ", this.Comments)}{NL}" +
            $"---Story Info---{NL}" +
            $"Story Size: {this.Size}{NL}" +
            $"Story Priority: {this.Priority}{NL}" +
            $"Story Status: {this.StoryStatus}{NL}" +
            $"Assignee: {this.GetAssignee()}{NL}";
        }

        public string GetAssignee()
        {
            StringBuilder sb = new StringBuilder();

            if (this.Assignee == null) 
            { 
                sb.AppendLine("Assignee: none"); 
            }
            else 
            { 
                sb.AppendLine($"{this.Assignee.Name}"); 
            }

            return sb.ToString().Trim();
        }
    }
}