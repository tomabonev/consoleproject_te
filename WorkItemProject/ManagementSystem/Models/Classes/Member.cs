﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Member class that contains workitems , names and history
    /// </summary>
    public class Member : IMember
    {
        private string name;

        public Member(string name)
        {
            this.Name = name;
            this.WorkItems = new List<IWorkItem>();
            this.History = new List<EventHistory>();
        }

        public string Name
        {
            get => this.name;
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Validation.InvalidMemberName);
                }

                if (value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentException(Validation.InvalidMemberName);
                }
                this.name = value;
            }
        }

        public IList<IWorkItem> WorkItems { get; }

        public IList<EventHistory> History { get; }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            string output = $"------------------------{NL}" +
                   $"Member Name: {this.Name}{NL}" +
                   $"------------------------{NL}" +
                   $"{string.Join(" ", this.WorkItems.Select(items => items.ToString()))}{NL}" +
                   $"------{NL}" +
                   $"History:{NL}" +
                   $"------{NL}" +
                   $"{string.Join(" ", this.History)}{NL}";

            return output;
        }
    }
}