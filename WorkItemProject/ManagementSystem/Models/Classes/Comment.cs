﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    public class Comment : IComment
    {
        private string description;

        public Comment(IMember author, string description)
        {
            this.Author = author;
            this.Description = description;
            this.Date = DateTime.Now;
        }

        public IMember Author { get; }

        public string Description
        {
            get => this.description;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Validation.InvalidDescprition);
                }
                if (value.Length < 10 || value.Length > 50)
                {
                    throw new ArgumentException(Validation.InvalidCommentLength);
                }
                this.description = value;
            }
        }

        public DateTime Date { get; }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            string output = $"{this.Author.Name}{NL}" +
                   $"Has added the following description: {this.Description}{NL}" +
                   $"It was added at: {this.Date}{NL}";

            return output;
        }
    }
}