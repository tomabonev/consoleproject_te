﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;
    using System.Collections.Generic;

    /// <summary>
    /// Team class that contains boards, members, names and history
    /// </summary>
    public class Team : ITeam
    {
        private string name;

        public Team(string name)
        {
            this.Name = name;
            this.Members = new List<IMember>();
            this.Boards = new List<IBoard>();
            this.History = new List<EventHistory>();
        }

        public string Name
        {
            get => this.name;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Validation.InvalidTeamName);
                }
                if (value.Length < 3 || value.Length > 15)
                {
                    throw new ArgumentException(Validation.InvalidTeamNameLength);
                }
                this.name = value;
            }
        }

        public IList<IMember> Members { get; }

        public IList<IBoard> Boards { get; }

        public IList<EventHistory> History { get; }

        public void AddBoard(IBoard board)
        {
            if (this.Boards.Contains(board))
            {
                throw new ArgumentException(Validation.BoardExist);
            }
            this.Boards.Add(board);
        }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            string output = $"Name: {this.Name}----{NL}" +
                   $"Members: {this.Members}----{NL}" +
                   $"Boards: {this.Boards}----{NL}" +
                   $"History: {string.Join(" ", this.History)}----{NL}";

            return output;
        }
    }
}