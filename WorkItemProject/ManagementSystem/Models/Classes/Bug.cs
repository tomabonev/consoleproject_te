﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Abstract;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Bug class that inherits from abstract class workitems.
    /// </summary>
    public class Bug : WorkItem, IBug
    {
        public Bug(string title,
                   string description,
                   BugStatus bugStatus,
                   Severity severity,
                   Priority priority,
                   IList<string> stepsToReproduce)
            : base(title, description)
        {
            this.Status = bugStatus;
            this.Severity = severity;
            this.Priority = priority;
            this.StepsToReproduce = new List<string>();
            this.StepsToReproduce = stepsToReproduce;
        }

        public BugStatus Status { get; set; }

        public Severity Severity { get; set; }

        public Priority Priority { get; set; }

        public IList<string> StepsToReproduce { get; set; }

        public IMember Assignee { get; set; }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            return $"Type: Bug{NL}" +
            base.ToString() +
            $"Comment from the author: {string.Join(" ", this.Comments)}{NL}" +
            $"---Bug Info---{NL}" +
            $"Bug Status: {this.Status}{NL}" +
            $"Bug Severity: {this.Severity}{NL}" +
            $"Bug Priority: {this.Priority}{NL}" +
            $"------{NL}" +
            $"Assignee: {this.GetAssignee()}{NL}" +
            $"------{NL}" +
            $"StepsToReproduce: {NL}" +
            $"{GetStepToProduce()}{NL}" +
            $"------{NL}";
        }

        public string GetAssignee()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Assignee == null) { sb.AppendLine("none"); }
            else { sb.AppendLine($"{this.Assignee.Name}"); }
            return sb.ToString().Trim();
        }

        public string GetStepToProduce()
        {
            string NL = Environment.NewLine;
            StringBuilder sb = new StringBuilder();

            if (this.StepsToReproduce.Count == 0) 
            { 
                sb.AppendLine("none"); 
            }
            else { sb.AppendLine($"{string.Join(NL, this.StepsToReproduce)}"); }

            return sb.ToString().Trim();
        }
    }
}