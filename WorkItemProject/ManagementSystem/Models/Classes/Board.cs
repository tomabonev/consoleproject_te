﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Board class that contains workitems , names and history
    /// </summary>
    public class Board : IBoard
    {
        private string name;
        private readonly List<IWorkItem> workItems;

        public Board(string name)
        {
            this.Name = name;
            this.workItems = new List<IWorkItem>();
            this.History = new List<EventHistory>();
        }

        public string Name
        {
            get => this.name;
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Validation.InvalidBoardName);
                }
                if (value.Length < 5 || value.Length > 10)
                {
                    throw new ArgumentException(Validation.InvalidBoardName);
                }
                this.name = value;
            }
        }

        public IReadOnlyList<IWorkItem> WorkItems { get => this.workItems; }

        public IList<EventHistory> History { get; }

        public void Add(IWorkItem item)
        {
            this.workItems.Add(item);
        }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            var output = $"Name: {this.Name}{NL}" +
                   $"Work Items: {string.Join(" ", this.WorkItems)}{NL}" +
                   $"History: {string.Join(" ", this.History)}{NL}";

            return output;
        }
    }
}