﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Abstract;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Feedback class that inherits from abstract class workitems.
    /// </summary>
    public class Feedback : WorkItem, IFeedBack
    {
        private int rating;

        public Feedback(string title,
                        string description,
                        FeedbackStatus feedBackStatus,
                        int rating)
            : base(title, description)
        {
            this.FeedbackStatus = feedBackStatus;
            this.Rating = rating;
        }

        public FeedbackStatus FeedbackStatus { get; set; }

        public int Rating
        {
            get => this.rating;
            set
            {
                if (value < 0 || value > 5)
                {
                    throw new ArgumentException(Validation.InvalidRating);
                }
                this.rating = value;
            }
        }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            string output = $"Type: Feedback{NL}" +
            base.ToString() +
            $"---Feedback Info---{NL}" +
            $"Feedback status: {this.FeedbackStatus}{NL}" +
            $"Feedback Rating: {this.Rating}{NL}";

            return output;
        }
    }
}