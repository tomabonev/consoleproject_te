﻿namespace ManagementSystem.Models.Classes
{
    using System;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Event History that contains description and date time. 
    /// </summary>
    public class EventHistory : IEventHistory
    {
        public EventHistory(string description)
        {
            this.Description = description;
            this.DateTime = DateTime.Now;
        }

        public string Description { get; set; }

        public DateTime DateTime { get; }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            string output = $"{this.Description}{NL}" +
                   $"Added on: {this.DateTime}{NL}";

            return output;
        }
    }
}