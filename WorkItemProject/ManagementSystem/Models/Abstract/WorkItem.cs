﻿namespace ManagementSystem.Models.Abstract
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Abastract class that includes all the work items and information for them.
    /// </summary>
    public abstract class WorkItem : IWorkItem
    {
        private static int id = 1;
        protected string title;
        protected string description;

        public WorkItem(string title, string description)
        {
            this.Id = id++;
            this.Title = title;
            this.Description = description;
            this.History = new List<EventHistory>();
            this.Comments = new List<Comment>();
        }

        public int Id { get; }

        public string Title
        {
            get => this.title;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Validation.InvalidTitle);
                }
                if (value.Length < 10 || value.Length > 50)
                {
                    throw new ArgumentException(Validation.InvalidTitleLength);
                }
                this.title = value;
            }
        }

        public string Description
        {
            get => this.description;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Validation.InvalidDescription);
                }
                if (value.Length < 10 || value.Length > 500)
                {
                    throw new ArgumentException(Validation.InvalidDescriptionLength);
                }
                this.description = value;
            }
        }

        public IList<Comment> Comments { get; }

        public IList<EventHistory> History { get; }

        public override string ToString()
        {
            string NL = Environment.NewLine;
            return $"Work Item ID: {this.Id}{NL}" +
                   $"Title: {this.Title}{NL}" +
                   $"Description: {this.Description}{NL}";
        }
    }
}