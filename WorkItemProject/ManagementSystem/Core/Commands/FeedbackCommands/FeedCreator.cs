﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provide functionality for the creation of a feedback item in a specific team's board.
    /// </summary>
    public class FeedCreator : Command
    {
        /// <summary>
        /// Create an instance of <see cref="FeedCreator" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public FeedCreator(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Create unique feedback item and assign it to a specific team's board.
        /// </summary>
        /// <returns>Text containing feedback id and title was successfully created.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 6)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "6"));
            }

            string title = this.CommandParameters[0];

            if (!Enum.TryParse<FeedbackStatus>(this.CommandParameters[1], true, out FeedbackStatus feedbackStatus))
            {
                throw new ArgumentException(CommandValidations.InvalidEnumStatus);
            }

            if (!int.TryParse(this.CommandParameters[2], out int rating))
            {
                throw new ArgumentException(CommandValidations.InvalidEnumStatus);
            }

            // createfeedbackinboard feedbackfromAsen new 5 boardName ninjasteamrules this project is awesome
            string boardName = this.CommandParameters[3];
            string teamName = this.CommandParameters[4];
            string description = string.Join(" ", this.CommandParameters.Skip(5));
            IBoard board;

            try
            {
                board = this.Database.GetBoard(boardName, teamName);
            }
            catch (Exception)
            {
                board = null;
            }

            if (board == null)
            {
                throw new ArgumentException($"Board with name: {boardName} doesn't exist in Team!");
            }

            try
            {
                var newFeedback = this.Factory.CreateFeedback(title, description, feedbackStatus, rating);
                this.Database.AddFeedback(newFeedback);
                board.Add(newFeedback);
                newFeedback.History.Add(new EventHistory($"Feedback id {newFeedback.Id}: {newFeedback.Title} was successfully created."));
                output = $"Feedback id {newFeedback.Id}: {newFeedback.Title} was successfully created.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}