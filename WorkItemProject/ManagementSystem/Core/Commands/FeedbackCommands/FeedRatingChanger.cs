﻿namespace ManagementSystem.Core.Commands.FeedbackCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for changing the feedback rating status.
    /// </summary>
    public class FeedRatingChanger : Command
    {
        /// <summary>
        /// Create an instance of <see cref="FeedRatingChanger" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public FeedRatingChanger(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Identifies the unique feedback item and change the rating status of it.
        /// </summary>
        /// <returns>Text containing feedback with title was updated to a new rating status.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            if (string.IsNullOrEmpty(this.CommandParameters[0]) || string.IsNullOrEmpty(this.CommandParameters[1]))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            // changefeedbackrating ID 4
            int id = int.Parse(CommandParameters[0]);
            string feedback = CommandParameters[1];
            IFeedBack feedRating;

            try
            {
                feedRating = Database.GetFeedBack(id);
            }
            catch (Exception)
            {
                feedRating = null;
            }

            if (feedRating == null)
            {
                throw new ArgumentException(CommandValidations.NullItemInDatabase, "Feedback rating");
            }

            if (!int.TryParse(feedback, out int newRating))
            {
                throw new ArgumentException(CommandValidations.InvalidFeedbackRating);
            }

            try
            {
                feedRating.Rating = newRating;
                feedRating.History.Add(new EventHistory($"Feedback item with ID: {feedRating.Id} ," +
                    $" title: {feedRating.Title}was updated to a new rating: {feedRating.Rating}"));
                output = $"Feedback item with title: {feedRating.Title} was updated to a new rating: {feedRating.Rating}.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}