﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for changing the feedback status.
    /// </summary>
    public class FeedStatusChanger : Command
    {
        /// <summary>
        /// Create an instance of <see cref="FeedStatusChanger" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public FeedStatusChanger(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Identifies the unique feedback item and change the status of it.
        /// </summary>
        /// <returns>Text containing feedback with title was updated to a new status.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            if (string.IsNullOrEmpty(this.CommandParameters[0]) || string.IsNullOrEmpty(this.CommandParameters[1]))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            // changefeedbackstatus ID scheduled
            int id = int.Parse(CommandParameters[0]);
            string feedback = CommandParameters[1];
            IFeedBack feedToUpdate;

            try
            {
                feedToUpdate = Database.GetFeedBack(id);
            }
            catch (Exception)
            {
                feedToUpdate = null;
            }

            if (feedToUpdate == null)
            {
                throw new ArgumentException(CommandValidations.NullItemInDatabase, "Feedback status");
            }

            if (!Enum.TryParse<FeedbackStatus>(feedback, true, out FeedbackStatus newStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Feedback"));
            }

            try
            {
                feedToUpdate.FeedbackStatus = newStatus;
                feedToUpdate.History.Add(new EventHistory($"Feedback item with ID: {feedToUpdate.Id} ," +
                    $" title: {feedToUpdate.Title} was updated to a new feed: {feedToUpdate.FeedbackStatus}"));
                output = $"Feedback item with title: {feedToUpdate.Title} was updated to a new feed: {feedToUpdate.FeedbackStatus}.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}