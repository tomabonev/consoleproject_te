﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    ///  Provides functionality for adding a comment from a certain member to a work item.
    /// </summary>
    public class WorkItemComment : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="WorkItemComment" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public WorkItemComment(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Add specific comment from a certain member to a work item
        /// </summary>
        /// <returns>Text containing member's name has added a comment to the work item.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 3)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "3"));
            }

            // addcommenttoworkitem ID tomas bug is bigger than expected
            int id = int.Parse(CommandParameters[0]);

            IMember author;

            try
            {
                author = this.Database.GetMember(CommandParameters[1]);
            }
            catch (Exception)
            {
                author = null;
            }

            string comment = string.Join(" ", CommandParameters.Skip(2));

            if (author == null)
            {
                throw new ArgumentException($"This {author.Name} does not exist in our Database.");
            }

            IWorkItem item;

            try
            {
                item = this.Database.GetItem(id);
            }
            catch (Exception)
            {
                item = null;
            }

            try
            {
                item.Comments.Add(new Comment(author, comment));
                item.History.Add(new EventHistory($"Member: {author.Name} has added a comment: {string.Join(" ", comment)} to the work item."));
                output = $"Member: {author.Name} has added a comment: {string.Join(" ", comment)} to the work item.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}