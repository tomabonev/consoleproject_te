﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all bug items filtered by a severity status.
    /// </summary>
    public class SeveritySortList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="SeveritySortList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public SeveritySortList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bug items that are sorted by a severity status.
        /// </summary>
        /// <returns>Text containing bug items with the specific severity status</returns>
        public override string Execute()
        {
            string output;

            var severityBugs = this.Database.Bug;

            severityBugs = severityBugs.OrderBy(bugs => bugs.Severity).ToList();

            try
            {
                output = severityBugs.Count > 0
              ? string.Join(Environment.NewLine, severityBugs).Trim()
              : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}
