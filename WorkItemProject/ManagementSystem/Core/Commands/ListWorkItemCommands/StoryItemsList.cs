﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all story items.
    /// </summary>
    public class StoryItemsList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="StoryItemsList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public StoryItemsList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the story items.
        /// </summary>
        /// <returns>Text containing all the story items with their title and description.</returns>
        public override string Execute()
        {
            string output;
            var stories = this.Database.Story;

            try
            {
                output = stories.Count > 0
                ? string.Join(Environment.NewLine, stories).Trim()
                : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}
