﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Provides functionality for visualization of all bug and story items filtered by a priority status.
    /// </summary>
    public class PrioritySortList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="PrioritySortList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public PrioritySortList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bug and story items that are sorted by a priority status.
        /// </summary>
        /// <returns>Text containing bug and story items with the specific priority status</returns>
        public override string Execute()
        {
            string output;
            List<IPriority> priority = new List<IPriority>();


            var priorityBugs = this.Database.Bug;
            var priorityStories = this.Database.Story;

            foreach (var bug in priorityBugs)
            {
                priority.Add(bug);
            }

            foreach (var story in priorityStories)
            {
                priority.Add(story);
            }

            priority = priority.OrderByDescending(items => items.Priority).ToList();

            try
            {
                output = priority.Count > 0
              ? string.Join(Environment.NewLine, priority).Trim()
             : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}