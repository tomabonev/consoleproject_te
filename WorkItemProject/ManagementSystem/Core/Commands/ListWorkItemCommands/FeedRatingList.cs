﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all feedback items filtered by a rating status.
    /// </summary>
    /// <returns>the feedback rating sorted by descending.</returns>
    public class FeedRatingList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="FeedRatingList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public FeedRatingList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the feedback items filtered by a rating status.
        /// </summary>
        /// <returns>feedback: title ,rating ,status and description of the item.</returns>
        public override string Execute()
        {
            string output;

            try
            {
                var feedRating = this.Database.FeedBack;
                feedRating = feedRating.OrderByDescending(feed => feed.Rating).ToList();
                output = feedRating.Count > 0
              ? string.Join(Environment.NewLine, feedRating).Trim()
              : "There are no feed items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}