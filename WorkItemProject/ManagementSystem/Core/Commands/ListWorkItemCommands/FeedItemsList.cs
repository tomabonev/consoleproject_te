﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all feedback items
    /// </summary>
    public class FeedItemsList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="FeedItemsList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public FeedItemsList(IList<string> commandParameters)
       : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the feedback items.
        /// </summary>
        /// <returns>Text containing all the feedback items with their title and description.</returns>
        public override string Execute()
        {
            string output;

            try
            {
                var feedBacks = this.Database.FeedBack;
                output = feedBacks.Count > 0
              ? string.Join(Environment.NewLine, feedBacks).Trim()
              : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}