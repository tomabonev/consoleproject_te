﻿
namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of all bug, stories and feedback items filtered by a status.
    /// </summary>
    public class StatusItemsList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="StatusItemsList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public StatusItemsList(IList<string> commandParameters)
       : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bug, story and feedback items that are sorted by a status.
        /// </summary>
        /// <returns>Text containing bug,story and feedback items with the specific status</returns>
        public override string Execute()
        {
            string output;
            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            // listitemsbystatus active           

            string status = CommandParameters[0];

            bool bug = Enum.TryParse<BugStatus>(status, true, out BugStatus bugResult);
            bool story = Enum.TryParse<StoryStatus>(status, true, out StoryStatus storyResult);
            bool feedBack = Enum.TryParse<FeedbackStatus>(status, true, out FeedbackStatus feedBackResult);

            var bugsList = this.Database.Bug;
            var storiesList = this.Database.Story;
            var feedList = this.Database.FeedBack;

            try
            {
                if (bug)
                {
                    bugsList = bugsList.Where(bugStatus => bugStatus.Status == bugResult).ToList();

                    return bugsList.Count > 0
                  ? string.Join(Environment.NewLine, bugsList).Trim()
                  : $"There is no bug status: {status} in our Database.";
                }
                else if (story)
                {
                    storiesList = storiesList.Where(storyStatus => storyStatus.StoryStatus == storyResult).ToList();

                    return storiesList.Count > 0
                    ? string.Join(Environment.NewLine, storiesList).Trim()
                    : $"There is no story status: {status} in our Database.";
                }
                else if (feedBack)
                {
                    feedList = feedList.Where(feedBackStatus => feedBackStatus.FeedbackStatus == feedBackResult).ToList();

                    return feedList.Count > 0
                    ? string.Join(Environment.NewLine, feedList).Trim()
                    : $"There is no feedback status: {status} in our Database.";
                }

                output = $"There is no such status: {status} in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}