﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Provides functionality for visualization of all list items filtered by allocation.
    /// </summary>
    public class AssigneeList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="AssigneeList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public AssigneeList(IList<string> commandParameters)
       : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bugs or stories that are assgined to a specific member.
        /// </summary>
        /// <returns>Text containing all the bug and story items that are assigned to a specific member.</returns>
        public override string Execute()
        {
            List<IAssignee> assignees = new List<IAssignee>();
            string output;

            var assigneeBugs = this.Database.Bug;
            var assigneeStories = this.Database.Story;

            foreach (var bug in assigneeBugs)
            {
                assignees.Add(bug);
            }

            foreach (var story in assigneeStories)
            {
                assignees.Add(story);
            }

            try
            {
                assignees = assignees.OrderBy(items => items.Assignee.Name).ToList();
                output = assignees.Count > 0
              ? string.Join(Environment.NewLine, assignees).Trim()
              : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}