﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all story items filtered by a size status.
    /// </summary>
    public class StorySizeList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="StorySizeList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public StorySizeList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the story items that are sorted by a size status.
        /// </summary>
        /// <returns>Text containing story items with the specific size status</returns>
        public override string Execute()
        {
            string output;

            try
            {
                var sizeStory = this.Database.Story;

                sizeStory = sizeStory.OrderBy(story => story.Size).ToList();

                output = sizeStory.Count > 0
              ? string.Join(Environment.NewLine, sizeStory).Trim()
              : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}
