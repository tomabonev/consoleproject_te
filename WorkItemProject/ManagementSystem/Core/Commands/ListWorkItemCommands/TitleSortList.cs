﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Provides functionality for visualization of all bug, stories and feedback items filtered by a title.
    /// </summary>
    public class TitleSortList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="TitleSortList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public TitleSortList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bug,story and feedback items sorted by a title.
        /// </summary>
        /// <returns>Text containing the title of the specific work item.</returns>
        public override string Execute()
        {
            string output;
            List<IWorkItem> titleSorted = new List<IWorkItem>();

            var titleBugs = this.Database.Bug;
            var titleStories = this.Database.Story;
            var titleFeedback = this.Database.FeedBack;

            foreach (var bug in titleBugs)
            {
                titleSorted.Add(bug);
            }

            foreach (var story in titleStories)
            {
                titleSorted.Add(story);
            }

            foreach (var feed in titleFeedback)
            {
                titleSorted.Add(feed);
            }

            titleSorted = titleSorted.OrderBy(title => title.Title).ToList();

            try
            {
                output = titleSorted.Count > 0
              ? string.Join(Environment.NewLine, titleSorted).Trim()
              : "There are no items in our Database.";

            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}