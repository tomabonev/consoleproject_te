﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of all available work items : bugs, stories, feedback.
    /// </summary>
    public class AllCommandsList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="AllCommandsList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public AllCommandsList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bugs, feedback, stories.
        /// </summary>
        /// <returns>Text containing work items: title and description.</returns>
        public override string Execute()
        {
            List<IWorkItem> items = new List<IWorkItem>();
            string output;

            foreach (var bug in this.Database.Bug)
            {
                if (bug == null)
                {
                    throw new ArgumentException(string.Format(CommandValidations.EmptyItemInDatabase, "bug"));
                }

                items.Add(bug);
            }

            foreach (var story in this.Database.Story)
            {
                if (story == null)
                {
                    throw new ArgumentException(string.Format(CommandValidations.EmptyItemInDatabase, "story"));
                }

                items.Add(story);
            }

            foreach (var feedBack in this.Database.FeedBack)
            {
                if (feedBack == null)
                {
                    throw new ArgumentException(string.Format(CommandValidations.EmptyItemInDatabase, "feedback"));
                }

                items.Add(feedBack);
            }

            try
            {
                output = items.Count > 0
                ? string.Join(Environment.NewLine, items).Trim()
                : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}