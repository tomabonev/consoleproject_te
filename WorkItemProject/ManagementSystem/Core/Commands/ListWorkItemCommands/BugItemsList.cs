﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all bug items
    /// </summary>
    public class BugItemsList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="BugItemsList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public BugItemsList(IList<string> commandParameters)
        : base(commandParameters)
        {
        }
        /// <summary>
        /// Visualize all the bug item.
        /// </summary>
        /// <returns>Text containing all the bugs with their title and description.</returns>
        public override string Execute()
        {
            string output;     
            
            try
            {
                var bugs = this.Database.Bug;
                output = bugs.Count > 0
              ? string.Join(Environment.NewLine, bugs).Trim()
              : "There are no items in our Database.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}