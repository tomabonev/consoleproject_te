﻿namespace ManagementSystem.Core.Commands.ListWorkItemCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of all list items filtered by allocation and status.
    /// </summary>
    public class AssigneeStatusList : Command
    {
        /// <summary>
        /// Create an instance of <see cref="AssigneeStatusList" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public AssigneeStatusList(IList<string> commandParameters)
       : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all the bugs or stories that are assgined to a specific member and sort them by a Status.
        /// </summary>
        /// <returns>Text containing all the bug and story that are assigned to a specific member and sorted by status.</returns>
        public override string Execute()
        {
            // listbystatusandassignee fixed tomas
            if (CommandParameters.Count != 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            string status = CommandParameters[0];
            string name = CommandParameters[1];

            bool bugEnum = Enum.TryParse<BugStatus>(status, true, out BugStatus bugResult);
            bool storyEnum = Enum.TryParse<StoryStatus>(status, true, out StoryStatus storyResult);
            bool feedEnum = Enum.TryParse<FeedbackStatus>(status, true, out FeedbackStatus feedBackResult);

            var bugsList = this.Database.Bug;
            var storiesList = this.Database.Story;
            var feedList = this.Database.FeedBack;

            string output = null;

            try
            {
                if (bugEnum)
                {
                    bugsList = bugsList.Where(bug => bug.Status == bugResult && bug.Assignee.Name == name).ToList();

                    output = bugsList.Count > 0
                    ? string.Join(Environment.NewLine, bugsList).Trim()
                    : $"There is no bug item with {name} or {status} in our Database.";

                    return output;
                }
                else if (storyEnum)
                {
                    storiesList = storiesList.Where(story => story.StoryStatus == storyResult && story.Assignee.Name == name).ToList();

                    output = storiesList.Count > 0
                  ? string.Join(Environment.NewLine, storiesList).Trim()
                  : $"There is no story item with {name} or {status} in our Database.";                 
                }
                else if (feedEnum)
                {
                    throw new ArgumentException("There is no feed status and assignee for feedback.");
                }
                else
                {
                    throw new ArgumentException($"There is no item with { name } or { status} in our Database.");
                }
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}