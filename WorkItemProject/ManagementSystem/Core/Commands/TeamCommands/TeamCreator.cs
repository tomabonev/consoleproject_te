﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provide functionality for the creation of a team.
    /// </summary>
    public class TeamCreator : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="TeamCreator" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public TeamCreator(IList<string> commandParameters)
    : base(commandParameters)
        {
        }

        /// <summary>
        /// Create a specific team
        /// </summary>
        /// <returns>Text containing team with the name was created.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            // createteam ninjasteamrules
            string teamName = this.CommandParameters[0];

            if (string.IsNullOrEmpty(teamName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            ITeam team;

            try
            {
                team = this.Database.GetTeam(teamName);
            }
            catch (Exception)
            {
                team = null;
            }

            if (team != null)
            {
                throw new ArgumentException("This team name allready exist in our Database.");
            }

            try
            {
                var createTeam = this.Factory.CreateTeam(teamName);
                createTeam.History.Add(new EventHistory($"Team with the name: {createTeam.Name} was created."));
                this.Database.AddTeam(createTeam);
                output = $"Team with the name: {createTeam.Name} was created.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();

            }

            return output;
        }
    }
}