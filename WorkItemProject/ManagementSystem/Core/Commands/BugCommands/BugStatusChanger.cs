﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for changing the bug status.
    /// </summary>
    public class BugStatusChanger : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="BugStatusChanger" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public BugStatusChanger(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Identifies the unique bug item and change the status of it.
        /// </summary>
        /// <returns>Text containing bug with title was updated to a new status.</returns>
        public override string Execute()
        {
            string output;
            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            if (string.IsNullOrEmpty(this.CommandParameters[0]) || string.IsNullOrEmpty(this.CommandParameters[1]))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            // changebugstatus ID active
            int id = int.Parse(CommandParameters[0]);
            string status = CommandParameters[1];
            IBug bugToUpdate;

            try
            {
                bugToUpdate = Database.GetBug(id);
            }
            catch (Exception)
            {
                bugToUpdate = null;
            }

            if (bugToUpdate == null)
            {
                throw new ArgumentException(CommandValidations.NullItemInDatabase, "Bug status");
            }

            if (!Enum.TryParse<BugStatus>(status, true, out BugStatus newStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Bug"));
            }

            try
            {
                bugToUpdate.Status = newStatus;
                bugToUpdate.History.Add(new EventHistory($"Bug item with ID: {bugToUpdate.Id} , " +
                    $"title: {bugToUpdate.Title} was updated to status: {bugToUpdate.Status}"));
                output = $"Bug item with title: {bugToUpdate.Title} was updated to status: {bugToUpdate.Status}";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}