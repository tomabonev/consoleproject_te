﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provide functionality for the creation of a bug item in a specific team's board.
    /// </summary>
    public class BugCreator : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="BugCreator" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public BugCreator(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Create unique bug item and assign it to a specific team's board.
        /// </summary>
        /// <returns>Text containing bugs id with title was successfully created.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 7)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "7"));
            }

            // createbuginboard asenegotin boardName ninjasteamrules fixed critical high asen is very good developer
            string title = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];
            string teamName = this.CommandParameters[2];

            if (!Enum.TryParse<BugStatus>(this.CommandParameters[3], true, out BugStatus bugStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Bug"));
            }

            if (!Enum.TryParse<Severity>(this.CommandParameters[4], true, out Severity severityStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Severity"));
            }

            if (!Enum.TryParse<Priority>(this.CommandParameters[5], true, out Priority priorityStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Priority"));
            }

            string description = string.Join(" ", this.CommandParameters.Skip(6));
            List<string> steps = this.GetStepsToReproduce();

            try
            {
                var bug = this.Factory.CreateBug(title, description, bugStatus, severityStatus, priorityStatus, steps);
                this.Database.AddBug(bug);
                var board = this.Database.GetBoard(boardName, teamName);
                board.Add(bug);
                bug.History.Add(new EventHistory($"Bug id {bug.Id}: {bug.Title} was successfully created."));
                output = $"Bug id {bug.Id}: {bug.Title} was successfully created.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }

        /// <summary>
        /// Describe the needed steps to reproduce for dealing with the bug.
        /// </summary>
        /// <returns>Step has been added successfully</returns>
        private List<string> GetStepsToReproduce()
        {
            List<string> steps = new List<string>();

            while (true)
            {
                Console.WriteLine($"Describe the issue number: {steps.Count + 1}. Please type 'finish' when you are ready.");

                string input = Console.ReadLine();

                if (input == "finish")
                {
                    Console.WriteLine("Steps to reproduce has been successfully submitted.");
                    break;
                }

                Console.WriteLine("Step has been added sucessfully");
                steps.Add(input);
            }

            return steps;
        }
    }
}
