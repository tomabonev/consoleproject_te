﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for changing the bug severity status.
    /// </summary>
    public class BugSeverityChanger : Command
    {
        /// <summary>
        /// Create an instance of <see cref="BugSeverityChanger" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public BugSeverityChanger(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Identifies the unique bug item and change the severity status of it.
        /// </summary>
        /// <returns>Text containing bug with title was updated to a new severity status.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            if (string.IsNullOrEmpty(this.CommandParameters[0]) || string.IsNullOrEmpty(this.CommandParameters[1]))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            // changebugseverity ID major
            int id = int.Parse(CommandParameters[0]);
            string severity = CommandParameters[1];
            IBug bugToUpdate;

            try
            {
                bugToUpdate = Database.GetBug(id);
            }
            catch (Exception)
            {
                bugToUpdate = null;
            }

            if (bugToUpdate == null)
            {
                throw new ArgumentException(CommandValidations.NullItemInDatabase, "Bug severity");
            }

            if (!Enum.TryParse<Severity>(severity, true, out Severity newStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Severity"));
            }

            try
            {
                bugToUpdate.Severity = newStatus;
                bugToUpdate.History.Add(new EventHistory($"Bug item with ID: {bugToUpdate.Id} ," +
                    $" title: {bugToUpdate.Title} was updated to a new severity status: {bugToUpdate.Severity}"));
                output = $"Bug item with title: {bugToUpdate.Title} was updated to a new severity status: {bugToUpdate.Severity}.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}