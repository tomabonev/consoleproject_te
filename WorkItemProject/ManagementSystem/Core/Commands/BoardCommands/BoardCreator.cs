﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provide functionality for the creation of a board in a specific team.
    /// </summary>
    public class BoardCreator : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="BoardCreator" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public BoardCreator(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Create board with a board name and assign it to a specific team.
        /// </summary>
        /// <returns>Text containing board with the name was created.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            // createboard tickets ninjasteamrules          
            string boardName = this.CommandParameters[0];
            string teamName = this.CommandParameters[1];

            if (string.IsNullOrEmpty(boardName) || string.IsNullOrEmpty(teamName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            IBoard board;

            try
            {
                board = this.Database.GetBoard(boardName, teamName);
            }
            catch (Exception)
            {
                board = null;
            }

            if (board != null)
            {
                throw new ArgumentException(CommandValidations.BoardExists);
            }

            try
            {
                var newBoard = this.Factory.CreateBoard(boardName);
                this.Database.AddBoard(newBoard, teamName);
                newBoard.History.Add(new EventHistory($"Board with name:{newBoard.Name} was created."));
                output = $"Board with the name: {newBoard.Name} was created.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}
