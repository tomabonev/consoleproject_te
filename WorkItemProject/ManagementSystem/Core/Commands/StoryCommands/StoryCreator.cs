﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provide functionality for the creation of a story item in a specific team's board.
    /// </summary>
    public class StoryCreator : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="StoryCreator" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public StoryCreator(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Create unique story item and assign it to a specific team's board.
        /// </summary>
        /// <returns>Text containing story id and title was successfully created.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 7)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "7"));
            }

            // createstoryinboard morningstar small high inprogress boardData  ninjasteamrules lucifer morning star
            string title = this.CommandParameters[0];

            if (!Enum.TryParse<Size>(this.CommandParameters[1], true, out Size sizeStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Size"));
            }

            if (!Enum.TryParse<Priority>(this.CommandParameters[2], true, out Priority priorityStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Priority"));
            }

            if (!Enum.TryParse<StoryStatus>(this.CommandParameters[3], true, out StoryStatus storyStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Story"));
            }

            string boardName = this.CommandParameters[4];
            string teamName = this.CommandParameters[5];
            string description = string.Join(" ", this.CommandParameters.Skip(6));

            IBoard board;

            try
            {
                board = this.Database.GetBoard(boardName, teamName);
            }
            catch (Exception)
            {
                board = null;
            }

            if (board == null)
            {
                throw new ArgumentException(CommandValidations.BoardExists);
            }

            try
            {
                var newStory = this.Factory.CreateStory(title, description, sizeStatus, priorityStatus, storyStatus);
                this.Database.AddStory(newStory);
                board.Add(newStory);
                newStory.History.Add(new EventHistory($"Story id {newStory.Id}: {newStory.Title} was successfully created."));
                output = $"Story id {newStory.Id}: {newStory.Title} was successfully created.";
            }
            catch (Exception ex)
            {

                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}