﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for changing the story size status.
    /// </summary>
    public class StorySizeChanger : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="StorySizeChanger" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public StorySizeChanger(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Identifies the unique story item and change the size status of it.
        /// </summary>
        /// <returns>Text containing story with a title was updated to a new size status</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            if (string.IsNullOrEmpty(this.CommandParameters[0]) || string.IsNullOrEmpty(this.CommandParameters[1]))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            // changestorysize ID large
            int id = int.Parse(this.CommandParameters[0]);
            string size = this.CommandParameters[1];

            IStory sizeToUpdate;

            try
            {
                sizeToUpdate = Database.GetStory(id);
            }
            catch (Exception)
            {
                sizeToUpdate = null;
            }

            if (sizeToUpdate == null)
            {
                throw new ArgumentException(CommandValidations.NullItemInDatabase, "Story size");
            }

            if (!Enum.TryParse<Size>(size, true, out Size newStatus))
            {
                throw new ArgumentException(string.Format(CommandValidations.InvalidEnumStatus, "Size"));
            }

            try
            {
                sizeToUpdate.Size = newStatus;
                sizeToUpdate.History.Add(new EventHistory($"Story item with title: {sizeToUpdate.Title} " +
                    $"was updated to a new size: {sizeToUpdate.Size}"));
                output = $"Story item with title: {sizeToUpdate.Title} was updated to a new size: {sizeToUpdate.Size}.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}