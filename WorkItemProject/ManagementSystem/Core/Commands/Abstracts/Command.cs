﻿namespace ManagementSystem.Core.Commands.Abstracts
{
    using System.Collections.Generic;
    using ManagementSystem.Core.Contracts;

    /// <summary>
    /// Abstract Command class with instances of Database,Factory and Execute().
    /// </summary>
    public abstract class Command : ICommand
    {
        protected Command(IList<string> commandParameters)
        {
            this.CommandParameters = new List<string>(commandParameters);
        }

        protected IList<string> CommandParameters
        {
            get;
        }

        protected IDatabase Database
        {
            get
            {
                return Core.Database.Database.Instance;
            }
        }

        protected IFactory Factory
        {
            get
            {
                return Factories.Factory.Instance;
            }
        }

        public abstract string Execute();
    }
}
