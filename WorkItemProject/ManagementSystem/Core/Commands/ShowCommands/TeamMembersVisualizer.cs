﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of all team members.
    /// </summary>
    public class TeamMembersVisualizer : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="TeamMembersVisualizer" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public TeamMembersVisualizer(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all team members
        /// </summary>
        /// <returns>Text containing the member name, wokritems and history information</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            // showteammembers ninjasteamrules
            string teamName = this.CommandParameters[0];

            if (string.IsNullOrEmpty(teamName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            ITeam team;

            try
            {
                team = this.Database.GetTeam(teamName);
            }
            catch (Exception)
            {
                team = null;
            }

            if (team == null)
            {
                throw new ArgumentException(string.Format(CommandValidations.EmptyMemberInDatabase, "member"));
            }

            try
            {
                output = team.Members.Any() ?
               string.Join(' ', team.Members.Select(a => a.ToString()))
               : "Team doesn't have any members.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}