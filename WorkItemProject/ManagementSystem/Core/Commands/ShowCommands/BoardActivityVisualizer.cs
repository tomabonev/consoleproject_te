﻿namespace ManagementSystem.Core.Commands.ShowCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of a specific board activity
    /// </summary>
    public class BoardActivityVisualizer : Command
    {
        /// <summary>
        /// Create an instance of <see cref="BoardActivityVisualizer" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public BoardActivityVisualizer(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize specific board activity for a individual team
        /// </summary>
        /// <returns>Text containing the board history log</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            // showboardsactivity boarddata  ninjasteamrules
            string boardName = CommandParameters[0];
            string teamName = CommandParameters[1];

            if (string.IsNullOrEmpty(boardName) || string.IsNullOrEmpty(teamName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            IBoard board;

            try
            {
                board = Database.GetBoard(boardName, teamName);
            }
            catch (Exception)
            {
                board = null;
            }

            if (board == null)
            {
                throw new ArgumentException(string.Format(CommandValidations.EmptyMemberInDatabase, "board"));
            }

            if (board.History.Count == 0)
            {
                return "There are no registered activity for this board.";
            }

            try
            {
                output = string.Join(" ", board.History.Select(x => x.ToString()));
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();              
            }

            return output;
        }
    }
}