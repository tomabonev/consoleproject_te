﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provide functionality for visualization of a specific member activity
    /// </summary>
    public class MemberActivityVisualizer : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="MemberActivityVisualizer"class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public MemberActivityVisualizer(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize specific members activity
        /// </summary>
        /// <returns>Text containing specific member history log.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            // showpersonsactivity tomas
            string memberName = CommandParameters[0];

            if (string.IsNullOrEmpty(memberName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            IMember member;

            try
            {
                member = Database.GetMember(memberName);
            }
            catch (Exception)
            {
                member = null;
            }

            if (member == null)
            {
                throw new ArgumentException(string.Format(CommandValidations.EmptyMemberInDatabase, "member"));
            }

            if (member.History.Count == 0)
            {
                return "There are no registered activity for this member.";
            }

            try
            {
                output = string.Join(" ", member.History.Select(x => x.ToString()));
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}