﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provide functionality for visualization of all teams.
    /// </summary>
    public class ShowAllTeams : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="ShowAllTeams" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public ShowAllTeams(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all teams.
        /// </summary>
        /// <returns>Text containing all teams sorted alphabetically by name.</returns>
        public override string Execute()
        {
            string output;

            try
            {
                output = this.Database.Teams.Count > 0
               ? string.Join(Environment.NewLine, this.Database.Teams.Select(name => name.Name)).Trim()
               : "There are no registered teams.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}
