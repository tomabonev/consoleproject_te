﻿namespace ManagementSystem.Core.Commands.TeamCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of a specific team.
    /// </summary>
    public class TeamActivityVisualizer : Command
    {
        /// <summary>
        /// Create an instance of<see cref="TeamActivityVisualizer" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public TeamActivityVisualizer(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize specific team activity
        /// </summary>
        /// <returns>Text containing the team history</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            // showteamsactivity ninjasteamrules
            string teamName = CommandParameters[0];

            if (string.IsNullOrEmpty(teamName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            ITeam team;

            try
            {
                team = Database.GetTeam(teamName);
            }
            catch (Exception)
            {
                team = null;
            }

            if (team == null)
            {
                throw new ArgumentException(string.Format(CommandValidations.EmptyMemberInDatabase, "team"));
            }

            if (team.History.Count == 0)
            {
                return "There are no registered activity for this team.";
            }

            try
            {
                output = string.Join(" ", team.History.Select(x => x.ToString()));
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}