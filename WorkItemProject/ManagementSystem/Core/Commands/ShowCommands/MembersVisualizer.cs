﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;

    /// <summary>
    /// Provides functionality for visualization of all members.
    /// </summary>
    public class MembersVisualizer : Command
    {
        /// <summary>
        /// Create an instance of <see cref="MembersVisualizer" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public MembersVisualizer(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize all members
        /// </summary>
        /// <returns>Text containing all members with title and description.</returns>
        public override string Execute()
        {
            string output;

            try
            {
                output = this.Database.Members.Count > 0
               ? string.Join(Environment.NewLine, this.Database.Members).Trim()
               : "There are no registered members.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}