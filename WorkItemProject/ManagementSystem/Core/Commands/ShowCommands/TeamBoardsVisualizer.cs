﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality for visualization of a specific boards.
    /// </summary>
    public class TeamBoardsVisualizer : Command
    {
        /// <summary>
        /// Creates an instance of <see cref="TeamBoardsVisualizer" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public TeamBoardsVisualizer(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Visualize the existing boards of a certain team.
        /// </summary>
        /// <returns>Text containing the board description, the board items and the history of the board.</returns>
        public override string Execute()
        {
            string output;

            // showteamboards ninjasteamrules
            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            string teamName = CommandParameters[0];

            if (string.IsNullOrEmpty(teamName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            ITeam team;

            try
            {
                team = this.Database.GetTeam(teamName);
            }
            catch (Exception)
            {
                team = null;
            }

            if (team == null)
            {
                throw new ArgumentException(string.Format(CommandValidations.EmptyMemberInDatabase, "team"));
            }

            try
            {
                output = team.Boards.Any() ?
                string.Join(' ', team.Boards.Select(a => a.Name))
                : "Team doesn't have any boards.";

            }
            catch (Exception ex)
            {

                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}