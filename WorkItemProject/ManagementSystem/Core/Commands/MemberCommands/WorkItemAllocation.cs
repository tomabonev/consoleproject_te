﻿namespace ManagementSystem.Core.Commands.MemberCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides a functionality of allocation a specific workitem to a certain member.
    /// </summary>
    public class WorkItemAllocation : Command
    {
        /// <summary>
        /// Create an instance of <see cref="WorkItemAllocation" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public WorkItemAllocation(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Assign specific work item to a member.
        /// </summary>
        /// <returns>Text containing item title has been successfully assigned to a member with a board name.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 4)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "4"));
            }

            IBoard board;
            IWorkItem item;
            IMember member;

            try
            {
                board = this.Database.GetBoard(CommandParameters[0], CommandParameters[1]);
                item = this.Database.GetItem(int.Parse(CommandParameters[2]));
                member = this.Database.GetMember(CommandParameters[3]);
            }
            catch (Exception)
            {
                board = null;
                item = null;
                member = null;
            }
            // assignworkitemtoperson boarddata ninjasteamrules ID tomas

            if (member == null)
            {
                throw new ArgumentException($"This {member.Name} does not exist in our Database.");
            }

            if (item == null)
            {
                throw new ArgumentException($"This {item.Title} does not exist in our Database.");
            }

            if (item is IStory)
            {
                IStory story = (IStory)item;
                member.WorkItems.Add(story);
                story.Assignee = member;
            }
            else if (item is IBug)
            {
                IBug bug = (IBug)item;
                member.WorkItems.Add(bug);
                bug.Assignee = member;
            }
            else if (item is IFeedBack)
            {
                return $"This is just a Feedback. It cannot be assign to {member.Name}.";
            }

            try
            {
                board.History.Add(new EventHistory($"{board.Name} with {item.Title} has been assigned to {member.Name}."));
                item.History.Add(new EventHistory($"{item.Title} with {board.Name} has been assigned to {member.Name}."));
                member.History.Add(new EventHistory($"{member.Name} from {board.Name} has recieved an item: {item.Title}."));
                output = $"Item {item.Title} has been successfully assigned to {member.Name} with a board {board.Name}.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}