﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides functionality of allocating a specific member to a certain team.
    /// </summary>
    public class TeamMemberAllocation : Command
    {
        /// <summary>
        /// Create an instance of <see cref="TeamMemberAllocation" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public TeamMemberAllocation(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Assign a member to a certain team.
        /// </summary>
        /// <returns>Text containing the member name has been added to a team name successfully.</returns>
        public override string Execute()
        {
            string output;

            // addpersontoteam tomas ninjasteamrules
            if (CommandParameters.Count < 2)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "2"));
            }

            IMember member;
            ITeam team;

            try
            {
                member = this.Database.GetMember(CommandParameters[0]);
                team = this.Database.GetTeam(CommandParameters[1]);
            }
            catch (Exception)
            {
                member = null;
                team = null;
            }

            if (member == null)
            {
                throw new ArgumentException($"This {member.Name} does not exist in our Database.");
            }

            if (team == null)
            {
                throw new ArgumentException($"This {team.Name} does not exist in our Database.");
            }

            try
            {
                team.Members.Add(member);
                member.History.Add(new EventHistory($"The member: {member.Name} has been added to a team {team.Name} successfully."));
                output = $"The member: {member.Name} has been added to a team: {team.Name} successfully.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}