﻿namespace ManagementSystem.Core.Commands.MemberCommands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides a functionality of dislocation a specific workitem to a certain member.
    /// </summary>
    public class WorkItemDislocation : Command
    {
        public WorkItemDislocation(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Unassigned work item from a specific member
        /// </summary>
        /// <returns>Text containing item title has been successfully unassigned from a member with a board name.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 4)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "4"));
            }

            IBoard boardName;
            IWorkItem item;
            IMember member;

            // unassignworkitemtoperson boarddata ninjasteamrules ID tomas
            try
            {
                boardName = this.Database.GetBoard(this.CommandParameters[0], this.CommandParameters[1]);
                item = this.Database.GetItem(int.Parse(this.CommandParameters[2]));
                member = this.Database.GetMember(this.CommandParameters[3]);

            }
            catch (Exception)
            {
                boardName = null;
                item = null;
                member = null;
            }

            if (member == null)
            {
                throw new ArgumentException($"This {member.Name} does not exist in our Database.");
            }

            if (item == null)
            {
                throw new ArgumentException($"This {item.Title} does not exist in our Database.");
            }

            if (item is IStory)
            {
                IStory story = (IStory)item;
                story.Assignee = member;
                member.WorkItems.Remove(story);
            }

            if (item is IBug)
            {
                IBug bug = (IBug)item;
                bug.Assignee = member;
                member.WorkItems.Remove(bug);
            }

            if (item is IFeedBack)
            {
                return $"This is just a feedback. Item:{item.Title} can't be unassign to {member.Name}.";
            }

            try
            {
                boardName.History.Add(new EventHistory($"Board {boardName.Name} with item {item.Title} has been unassigned from {member.Name}."));
                item.History.Add(new EventHistory($"The item {item.Title} with a board {boardName.Name} has been unassigned from {member.Name}."));
                member.History.Add(new EventHistory($"The item {item.Title} of {member.Name} with a board {boardName.Name} has been unnasigned."));
                output = $"Item {item.Title} has been successfully unassigned from {member.Name} with a board {boardName.Name}.";
            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}