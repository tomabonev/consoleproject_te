﻿namespace ManagementSystem.Core.Commands
{
    using System;
    using System.Collections.Generic;
    using ManagementSystem.Core.Commands.Abstracts;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using ManagementSystem.Models.ValidationMsg;

    /// <summary>
    /// Provides a functionality of a member creation.
    /// </summary>
    public class MemberCreator : Command
    {
        /// <summary>
        /// Create an instance of a <see cref="MemberCreator" class./>
        /// </summary>
        /// <param name="commandParameters"></param>
        public MemberCreator(IList<string> commandParameters)
        : base(commandParameters)
        {
        }

        /// <summary>
        /// Create a new member.
        /// </summary>
        /// <returns>Text containing member with the name was created.</returns>
        public override string Execute()
        {
            string output;

            if (CommandParameters.Count < 1)
            {
                throw new ArgumentException(string.Format(CommandValidations.ValueLength, "1"));
            }

            // createmember toma
            string memberName = this.CommandParameters[0];

            if (string.IsNullOrEmpty(memberName))
            {
                throw new ArgumentException(CommandValidations.EmptyValue);
            }

            IMember databaseMember;

            try
            {
                databaseMember = this.Database.GetMember(memberName);
            }
            catch (Exception)
            {
                databaseMember = null;
            }

            if (databaseMember != null)
            {
                throw new ArgumentException(CommandValidations.ExistingMember);
            }

            try
            {
                var member = this.Factory.CreateMember(memberName);
                member.History.Add(new EventHistory($"Member with the name: {member.Name} was created."));
                this.Database.AddMember(member);
                output = $"Member with the name: {member.Name} was created.";

            }
            catch (Exception ex)
            {
                output = "There is an error: " + ex.ToString();
            }

            return output;
        }
    }
}