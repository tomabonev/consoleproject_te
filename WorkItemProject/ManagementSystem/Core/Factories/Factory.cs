﻿namespace ManagementSystem.Core.Factories
{
    using ManagementSystem.Core.Contracts;
    using ManagementSystem.Enums;
    using ManagementSystem.Models.Classes;
    using ManagementSystem.Models.Contracts;
    using System.Collections.Generic;

    /// <summary>
    /// Factory is creating the work items and return a new instances of them.
    /// </summary>
    public class Factory : IFactory
    {
        private static IFactory instance;

        public static IFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Factory();
                }

                return instance;
            }
        }

        public IBug CreateBug(string title, string description, BugStatus bugStatus, Severity severity, Priority priority, List<string> stepsToReproduce)
        {
            return new Bug(title, description, bugStatus, severity, priority, stepsToReproduce);
        }

        public IFeedBack CreateFeedback(string title, string description, FeedbackStatus feedBackStatus, int rating)
        {
            return new Feedback(title, description, feedBackStatus, rating);
        }

        public IStory CreateStory(string title, string description, Size size, Priority priority, StoryStatus storyStatus)
        {
            return new Story(title, description, size, priority, storyStatus);
        }

        public IBoard CreateBoard(string name)
        {
            return new Board(name);
        }

        public IMember CreateMember(string name)
        {
            return new Member(name);
        }

        public ITeam CreateTeam(string name)
        {
            return new Team(name);
        }
    }
}