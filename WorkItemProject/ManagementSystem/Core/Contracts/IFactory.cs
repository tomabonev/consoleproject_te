﻿using ManagementSystem.Enums;
using ManagementSystem.Models.Contracts;
using System.Collections.Generic;

namespace ManagementSystem.Core.Contracts
{
    public interface IFactory
    {
        IBug CreateBug(string title, string description,BugStatus bugStatus, Severity severity, Priority priority,List<string> stepsToReproduce);
        IFeedBack CreateFeedback(string title, string description, FeedbackStatus feedBackStatus, int rating);

        IStory CreateStory(string title, string description,Size size, Priority priority, StoryStatus storyStatus);

        IBoard CreateBoard(string name);

        IMember CreateMember(string name);

        ITeam CreateTeam(string name);

    }
}
