﻿namespace ManagementSystem.Core.Contracts
{
    /// <summary>
    /// Defines the functionality of the command line through a method ParseCommand().
    /// </summary>
    public interface ICommandParser
    {
        ICommand ParseCommand(string commandLine);
    }
}
