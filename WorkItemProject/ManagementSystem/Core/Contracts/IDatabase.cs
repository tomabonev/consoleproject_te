﻿namespace ManagementSystem.Core.Contracts
{
    using System.Collections.Generic;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Defines the functionality of the work items through specific methods and list collections.
    /// </summary>
    public interface IDatabase
    {
        IReadOnlyList<IMember> Members { get; }

        IReadOnlyList<ITeam> Teams { get; }

        IReadOnlyList<IStory> Story { get; }

        IReadOnlyList<IFeedBack> FeedBack { get; }

        IReadOnlyList<IBug> Bug { get; }

        IWorkItem GetItem(int Id);

        ITeam GetTeam(string teamName);

        IBoard GetBoard(string boardName, string teamName);

        IMember GetMember(string memberName);

        IBug GetBug(int id);

        IStory GetStory(int id);

        IFeedBack GetFeedBack(int id);

        void AddMember(IMember member);

        void AddTeam(ITeam team);

        void AddBoard(IBoard board, string teamName);

        void AddBug(IBug bug);

        void AddStory(IStory story);

        void AddFeedback(IFeedBack feedBack);
    }
}