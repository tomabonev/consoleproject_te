﻿namespace ManagementSystem.Core.Contracts
{
    /// <summary>
    /// Defines the functionality of a string method Execute().
    /// </summary>
    public interface ICommand
    {
        string Execute();
    }
}
