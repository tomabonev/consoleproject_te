﻿namespace ManagementSystem.Core.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}
