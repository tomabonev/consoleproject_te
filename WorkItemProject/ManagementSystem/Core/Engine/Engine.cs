﻿namespace ManagementSystem.Core
{
    using System;
    using ManagementSystem.Core.Contracts;
    using ManagementSystem.Core.Provider;

    /// <summary>
    /// The Engine class runs my console application
    /// </summary>
    public class Engine : IEngine
    {
        private const string Delimiter = "=================================";
        private readonly ICommandParser parser;

        static Engine()
        {
            Instance = new Engine();
        }

        private Engine()
        {
            this.parser = new CommandParser();
        }

        public static IEngine Instance { get; }

        /// <summary>
        /// Read and Process the input from the commandParser and print the result
        /// </summary>
        public void Run()
        {
            //Console.WriteLine("INFO: Enter 'help' to see all possible commands");

            while (true)
            {
                // Read -> Process -> Print -> Repeat
                string input = this.Read().ToLower();
                string result = this.Process(input);
                this.Print(result);
            }
        }

        /// <summary>
        /// Read the input data
        /// </summary>
        /// <returns></returns>
        private string Read()
        {
            return Console.ReadLine();
        }

        private string Process(string commandLine)
        {
            if (commandLine == "end")
                Environment.Exit(0);

            string result = null;

            try
            {
                var command = this.parser.ParseCommand(commandLine);
                result = $"{command.Execute()}{Environment.NewLine}{Delimiter}";
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                result = $"ERROR: {e.Message}";
            }

            return result;
        }

        private void Print(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}