﻿namespace ManagementSystem.Core.Provider
{
    using ManagementSystem.Core.Commands;
    using ManagementSystem.Core.Commands.FeedbackCommands;
    using ManagementSystem.Core.Commands.Help;
    using ManagementSystem.Core.Commands.ListWorkItemCommands;
    using ManagementSystem.Core.Commands.MemberCommands;
    using ManagementSystem.Core.Commands.ShowCommands;
    using ManagementSystem.Core.Commands.TeamCommands;
    using ManagementSystem.Core.Contracts;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Receive the commands from the console
    /// </summary>
    public class CommandParser : ICommandParser
    {

        public ICommand ParseCommand(string commandLine)
        {
            var lineParameters = commandLine
                .Trim()
                .ToLower()
                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string commandName = lineParameters[0];
            List<string> commandParameters = lineParameters.Skip(1).ToList();

            return commandName switch
            {
                "help" => new HelpInfo(commandParameters),
                "createmember" => new MemberCreator(commandParameters),
                "createteam" => new TeamCreator(commandParameters),
                "createboard" => new BoardCreator(commandParameters),
                "createbuginboard" => new BugCreator(commandParameters),
                "createfeedbackinboard" => new FeedCreator(commandParameters),
                "createstoryinboard" => new StoryCreator(commandParameters),
                "showallpeople" => new MembersVisualizer(commandParameters),
                "showpersonsactivity" => new MemberActivityVisualizer(commandParameters),
                "showallteams" => new ShowAllTeams(commandParameters),
                "showteamsactivity" => new TeamActivityVisualizer(commandParameters),
                "showteammembers" => new TeamMembersVisualizer(commandParameters),
                "showteamboards" => new TeamBoardsVisualizer(commandParameters),
                "showboardsactivity" => new BoardActivityVisualizer(commandParameters),
                "changebugpriority" => new BugPriorityChanger(commandParameters),
                "changebugseverity" => new BugSeverityChanger(commandParameters),
                "changebugstatus" => new BugStatusChanger(commandParameters),
                "changestorypriority" => new StoryPriorityChanger(commandParameters),
                "changestorysize" => new StorySizeChanger(commandParameters),
                "changestorystatus" => new StoryStatusChanger(commandParameters),
                "changefeedbackrating" => new FeedRatingChanger(commandParameters),
                "changefeedbackstatus" => new FeedStatusChanger(commandParameters),
                "assignworkitemtoperson" => new WorkItemAllocation(commandParameters),
                "unassignworkitemtoperson" => new WorkItemDislocation(commandParameters),
                "addcommenttoworkitem" => new WorkItemComment(commandParameters),
                "addpersontoteam" => new TeamMemberAllocation(commandParameters),
                "listallworkitems" => new AllCommandsList(commandParameters),
                "listbugitems" => new BugItemsList(commandParameters),
                "liststoryitems" => new StoryItemsList(commandParameters),
                "listsfeedbackitems" => new FeedItemsList(commandParameters),
                "listitemsbystatus" => new StatusItemsList(commandParameters),
                "listsortbypriority" => new PrioritySortList(commandParameters),
                "listsorttitle" => new TitleSortList(commandParameters),
                "listsortseverity" => new SeveritySortList(commandParameters),
                "listsortsize" => new StorySizeList(commandParameters),
                "listfeedrating" => new FeedRatingList(commandParameters),
                "listbyassignee" => new AssigneeList(commandParameters),
                "listbystatusandassignee" => new AssigneeStatusList(commandParameters),

                _ => throw new InvalidOperationException("Command does not exist")
            };
        }
    }
}