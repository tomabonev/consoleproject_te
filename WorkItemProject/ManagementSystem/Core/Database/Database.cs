﻿namespace ManagementSystem.Core.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ManagementSystem.Core.Contracts;
    using ManagementSystem.Models.Contracts;

    /// <summary>
    /// Database class that contains different item lists and methods
    /// </summary>
    public class Database : IDatabase
    {
        private static IDatabase instance;
        private readonly List<IMember> members = new List<IMember>();
        private readonly List<ITeam> teams = new List<ITeam>();
        private readonly List<IBug> bug = new List<IBug>();
        private readonly List<IStory> story = new List<IStory>();
        private readonly List<IFeedBack> feedback = new List<IFeedBack>();

        public static IDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Database();
                }

                return instance;
            }
        }

        private Database()
        {

        }

        public IReadOnlyList<IMember> Members { get => this.members; }

        public IReadOnlyList<ITeam> Teams { get => this.teams; }

        public IReadOnlyList<IBug> Bug { get => this.bug; }

        public IReadOnlyList<IFeedBack> FeedBack { get => this.feedback; }

        public IReadOnlyList<IStory> Story { get => this.story; }

        public IWorkItem GetItem(int Id)
        {
            var story = GetStory(Id);

            if (story != null)
            {
                return story;
            }

            var feedback = GetFeedBack(Id);

            if (feedback != null)
            {
                return feedback;
            }

            var bug = GetBug(Id);

            if (bug != null)
            {
                return bug;
            }

            return null;
        }

        public ITeam GetTeam(string teamName)
        {
            if (teamName == null)
            {
                throw new ArgumentException("Team name cannot be a null");
            }

            var item = teams.FirstOrDefault(team => team.Name == teamName);

            return item;
        }

        public IBug GetBug(int id)
        {
            var item = Bug.FirstOrDefault(bug => bug.Id == id);

            return item;
        }

        public IStory GetStory(int id)
        {
            var item = Story.FirstOrDefault(story => story.Id == id);

            return item;
        }

        public IFeedBack GetFeedBack(int id)
        {
            var item = FeedBack.FirstOrDefault(feedback => feedback.Id == id);

            return item;
        }

        public IBoard GetBoard(string boardName, string teamName)
        {
            var team = GetTeam(teamName);

            if (team == null)
            {
                throw new ArgumentException("There is no such team in our Database");
            }

            if (boardName == null)
            {
                throw new ArgumentException("There is no such board in our Database");
            }

            var item = team.Boards.FirstOrDefault(board => board.Name == boardName);

            return item;
        }

        public IMember GetMember(string memberName)
        {
            var item = Members.FirstOrDefault(member => member.Name == memberName);

            return item;
        }

        public void AddMember(IMember member)
        {
            if (member == null)
            {
                throw new ArgumentException("Member cannot be null in our Database");
            }

            var members = this.GetMember(member.Name);

            if (members != null)
            {
                throw new ArgumentException("There is a member with the same name in our Database");
            }

            this.members.Add(member);
        }

        public void AddTeam(ITeam team)
        {
            if (team == null)
            {
                throw new ArgumentException("Team cannot be null in our Database");
            }

            var teams = this.GetTeam(team.Name);

            if (teams != null)
            {
                throw new ArgumentException("There is a team with the same name in our Database");
            }

            this.teams.Add(team);
        }

        public void AddBug(IBug bug)
        {
            if (bug == null)
            {
                throw new ArgumentException("Bug cannot be null in our Database");
            }

            var bugs = this.GetBug(bug.Id);

            if (bugs != null)
            {
                throw new ArgumentException($"There is a bug with the same Id: {bug.Id} in our Database");
            }

            this.bug.Add(bug);
        }

        public void AddStory(IStory story)
        {
            if (story == null)
            {
                throw new ArgumentException("Story cannot be null in our Database");
            }

            var stories = this.GetStory(story.Id);

            if (stories != null)
            {
                throw new ArgumentException($"There is a story with the same Id: {story.Id} in our Database");
            }

            this.story.Add(story);
        }

        public void AddFeedback(IFeedBack feedBack)
        {
            if (feedBack == null)
            {
                throw new ArgumentException("Feedback cannot be null in our Database");
            }

            var feeds = this.GetFeedBack(feedBack.Id);

            if (feeds != null)
            {
                throw new ArgumentException($"There is a Feedback with the same Id: {feedBack.Id} in our Database");
            }

            this.feedback.Add(feedBack);
        }

        public void AddBoard(IBoard board, string teamName)
        {
            var team = this.GetTeam(teamName);

            if (team == null)
            {
                throw new ArgumentException($"There is a Team with the same name: {team.Name} in our Database");
            }

            if (board == null)
            {
                throw new ArgumentException("Board cannot be null in our Database");
            }

            team.AddBoard(board);
        }
    }
}