﻿namespace ManagementSystem
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    using ManagementSystem.Core;
    using ManagementSystem.Core.Commands.Help;
    using SimpleCMenu.Menu;

    /// <summary>
    /// Menu image implemented with a lot of delagates and magic
    /// </summary>
    class StartUp
    {
        static void Main()
        {
            string headerText = "  __  __                     _____           _                 " +
             Environment.NewLine + " |  \\/  |                   / ____|         | |                " +
             Environment.NewLine + " | \\  / | ___ _ __  _   _  | (___  _   _ ___| |_ ___ _ __ ___" +
             Environment.NewLine + " | |\\/| |/ _ \\ '_ \\| | | |  \\___ \\| | | / __| __/ _ \\ '_ ` _ \\" +
             Environment.NewLine + " | |  | |  __/ | | | |_| |  ____) | |_| \\__ \\ ||  __/ | | | | |" +
             Environment.NewLine + " |_|  |_|\\___|_| |_|\\__,_| |_____/ \\__, |___/\\__\\___|_| |_| |_|" +
             Environment.NewLine + "                                    __/ |    " +
             Environment.NewLine + "                                   |___/             ";

            Console.Clear();

            // Setup the menu
            ConsoleMenu mainMenu = new ConsoleMenu();
            mainMenu.HeaderColor = ConsoleColor.Yellow;
            mainMenu.ForeColor = ConsoleColor.White;
            mainMenu.CursorColor = ConsoleColor.Red;

            HelpInfo help = new HelpInfo(new List<string>());

            mainMenu.Header = headerText;
            mainMenu.SubTitle = "-------------------- Menu ----------------------";
            mainMenu.addMenuItem(0, "1. Help!", () => Help());
            mainMenu.addMenuItem(1, "2. Type command", mainMenu.hideMenu);
            mainMenu.addMenuItem(2, "3. Exit", Exit);

            // Display the menu
            mainMenu.hideMenu();
            mainMenu.showMenu();
            Engine.Instance.Run();
        }
        public static void Help()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("===========================");
            sb.AppendLine("------CREATE COMMANDS------");
            sb.AppendLine();
            sb.AppendLine("1.  createmember             => Create a new member");
            sb.AppendLine("2.  createteam               => Create a new team");
            sb.AppendLine("3.  createboard              => Create a new board in a team");
            sb.AppendLine("4.  createbuginboard         => Create a new bug in a board in a board");
            sb.AppendLine("5.  createfeedbackinboard    => Create a new feedback in a board");
            sb.AppendLine("6.  createstoryinboard       => Create a new story in a board");
            sb.AppendLine();
            sb.AppendLine("------SHOW COMMANDS-------");
            sb.AppendLine();
            sb.AppendLine("7.  showpersonsactivity      => Show a specific member activity");
            sb.AppendLine("8.  showallteams             => Show all available teams");
            sb.AppendLine("9.  showteamsactivity        => Show a specific team activity");
            sb.AppendLine("10. showteammembers          => Show members from a team");
            sb.AppendLine("11. showteamboards           => Show team's boards");
            sb.AppendLine("12. showboardsactivity       => Show a specific board activity");
            sb.AppendLine();
            sb.AppendLine("------CHANGE COMMANDS------");
            sb.AppendLine();
            sb.AppendLine("13. changebugpriority        => Change bug status priority");
            sb.AppendLine("14. changebugseverity        => Change bug status severity");
            sb.AppendLine("15. changebugstatus          => Change bug status");
            sb.AppendLine("16. changestorypriority      => Change story status priority");
            sb.AppendLine("17. changestorysize          => Change the story size");
            sb.AppendLine("18. changefeedbackrating     => Change the feedback size");
            sb.AppendLine("19. changefeedbackstatus     => Change the feedback status");
            sb.AppendLine();
            sb.AppendLine("------ADD COMMANDS------");
            sb.AppendLine();
            sb.AppendLine("22. addcommenttoworkitem     => Add a comment from a member to a specific item");
            sb.AppendLine("23. addpersontoteam          => Add a member to a team");
            sb.AppendLine();
            sb.AppendLine("------LIST COMMANDS------");
            sb.AppendLine();
            sb.AppendLine("24. listallworkitems         => List all work items");
            sb.AppendLine("25. listbugitems             => List all bug items");
            sb.AppendLine("26. liststoryitems           => List all story items");
            sb.AppendLine("27. listsfeedbackitems       => List all feedback items");
            sb.AppendLine("28. listitemsbystatus        => List all items sorted by status");
            sb.AppendLine("29. listsortbypriority       => List all items sorted by priority status");
            sb.AppendLine("30. listsortseverity         => List all items sorted by severity status");
            sb.AppendLine("31. listsorttitle            => List all items sorted by title");
            sb.AppendLine("32. listsortsize             => List all items sorted by size status");
            sb.AppendLine("33. listfeedrating           => List all feedback items sorted by rating");
            sb.AppendLine("34. listbyassignee           => List all items filtered by assignee member");
            sb.AppendLine("35. listbystatusandassignee  => List all items sorted by status and filtered by assignee member");
            sb.AppendLine();
            sb.AppendLine("------ASSIGN/UNASSIGN COMMANDS------");
            sb.AppendLine();
            sb.AppendLine("36. assignworkitemtoperson   => Assign an item to a specific member");
            sb.AppendLine("37. unassignworkitemtoperson => Unassign an item from a specific member");
            sb.AppendLine();
            sb.AppendLine("=================================");
            sb.AppendLine("Please type the required command:");
            sb.AppendLine();
     
            string output = sb.ToString().TrimEnd();

            Console.WriteLine(output);
        }

        public static void Exit()
        {
            Environment.Exit(0);
        }
    }
}