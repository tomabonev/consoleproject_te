# WorkItem Management System (Project)
**Team: Toma Bonev & Emiliyan Rubchev**

* [x] **Trello Site:** https://trello.com/b/VVhpmN8L/alpha-team-work-item-management

**Project Description**

**Design and implement a Work Item Management (WIM) Console Application.**

**Functional Requirements**

**Application should support multiple teams. Each team has name, members, and boards.**


**Member has name, list of work items and activity history.**
* [x] Name should be unique in the application
* [x] Name is a string between 5 and 15 symbols.
* [x] There are 3 types of work items: bug, story, and feedback.

**Board has name, list of work items and activity history.**
* [x] Name should be unique in the team
* [x] Name is a string between 5 and 10 symbols.
* [x] There are 3 types of work items: bug, story, and feedback.

 **Bug**
* [x] Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments, and history.
* [x] Title is a string between 10 and 50 symbols.
* [x] Description is a string between 10 and 500 symbols.
* [x] Steps to reproduce is a list of strings.
* [x] Priority is one of the following: High, Medium, Low
* [x] Severity is one of the following: Critical, Major, Minor
* [x] Status is one of the following: Active, Fixed
* [x] Assignee is a member from the team.
* [x] Comments is a list of comments (string messages with author).
* [x] History is a list of all changes (string messages) that were done to the bug.

**Story**
* [x] Story has ID, title, description, priority, size, status, assignee, comments, and history.
* [x] Title is a string between 10 and 50 symbols.
* [x] Description is a string between 10 and 500 symbols.
* [x] Priority is one of the following: High, Medium, Low
* [x] Size is one of the following: Large, Medium, Small
* [x] Status is one of the following: NotDone, InProgress, Done
* [x] Assignee is a member from the team.
* [x] Comments is a list of comments (string messages with author).
* [x] History is a list of all changes (string messages) that were done to the story.

**Feedback**
* [x] Feedback has ID, title, description, rating, status, comments, and history.
* [x] Title is a string between 10 and 50 symbols.
* [x] Description is a string between 10 and 500 symbols.
* [x] Rating is an integer.
* [x] Status is one of the following: New, Unscheduled, Scheduled, Done
* [x] Comments is a list of comments (string messages with author).
* [x] History is a list of all changes (string messages) that were done to the feedback.

**Note: IDs of work items should be unique in the application i.e. if we have a bug with ID X then
we cannot have Story of Feedback with ID X.**

**Operations**

**Application should support the following operations:**
* [x] Create a new person
* [x] Show all people
* [x] Show person's activity
* [x] Create a new team
* [x] Show all teams
* [x] Show team's activity
* [x] Add person to team
* [x] Show all team members
* [x] Create a new board in a team
* [x] Show all team boards
* [x] Show board's activity
* [x] Create a new Bug/Story/Feedback in a board
* [x] Change Priority/Severity/Status of a bug
* [x] Change Priority/Size/Status of a story
* [x] Change Rating/Status of a feedback
* [x] Assign/Unassign work item to a person
* [x] Add comment to a work item
* [x] List work items with options:
* [x] List all
* [x] Filter bugs/stories/feedback only
* [x] Filter by status and/or assignee
* [x] Sort by title/priority/severity/size/rating

**WorkItem Input Data**
* createmember tomas
* createmember asenka
* createmember yoanna
* createteam ninjasteamrules
* createteam svirepi
* createteam luvici
* createboard boarddata ninjasteamrules
* createboard mnogosme svirepi
* createboard gladnite luvici
* addpersontoteam tomas ninjasteamrules
* addpersontoteam asenka svirepi
* addpersontoteam yoanna luvici
* createbuginboard tozigomaxam boarddata ninjasteamrules fixed critical high toma is a very good developer
* pisna mi!
* oshte malko!
* finish
* createbuginboard tomaegotin boarddata ninjasteamrules fixed critical high toma is a very good developer
* otivam na plaja!
* na bara sum, sipvai!
* finish
* createbuginboard asenegotin mnogosme svirepi fixed critical high asen is a very good developer
* pushi mi se nargile!
* pushih veche!
* finish
* createbuginboard yoannaegotina gladnite luvici fixed critical high yoanna is a very good developer
* gladna sum!
* izqdoh surnata!
* finish
* assignworkitemtoperson boarddata ninjasteamrules 1 tomas
* unassignworkitemtoperson boarddata ninjasteamrules 1 tomas
* assignworkitemtoperson boarddata ninjasteamrules 2 tomas
* assignworkitemtoperson mnogosme svirepi 3 asenka
* assignworkitemtoperson gladnite luvici 4 yoanna
* createfeedbackinboard feedbackfromAsen new 5 boarddata ninjasteamrules this project is awesome
* assignworkitemtoperson boarddata ninjasteamrules 5 tomas
* createfeedbackinboard feedbackfromToma new 5 mnogosme svirepi this project is amazing
* assignworkitemtoperson mnogosme svirepi 6 asenka
* createfeedbackinboard feedbackfromYoanna new 5 gladnite luvici idavame po mnogo
* assignworkitemtoperson gladnite luvici 7 yoanna
* createstoryinboard morningstar small high done boarddata ninjasteamrules lucifer morning star
* assignworkitemtoperson boarddata ninjasteamrules 8 tomas
* createstoryinboard sunriseincuba small high done gladnite luvici watching the sinrise on the beach
* assignworkitemtoperson gladnite luvici 9 yoanna
* createstoryinboard sunsetinmalibu small high done mnogosme svirepi eating what we can find
* assignworkitemtoperson mnogosme svirepi 10 asenka
* addcommenttoworkitem 2 tomas bug is bigger than expected
* addcommenttoworkitem 8 tomas When the sun rise - up, it's time for action!
* addcommenttoworkitem 3 asenka tabacco is almost over
* addcommenttoworkitem 10 asenka Welcome to Miami!
* addcommenttoworkitem 4 yoanna I forgot that I am a vegeterian
* addcommenttoworkitem 9 yoanna My vacation in Cuba is great!
* changebugpriority 2 medium
* changebugseverity 3 major
* changebugstatus 4 active
* changefeedbackstatus 5 scheduled
* changefeedbackrating 6 4
* changefeedbackrating 7 4
* changestorystatus 8 inprogress
* changestorysize 9 large
* changestorypriority 10 medium
* showpersonsactivity tomas
* showpersonsactivity asenka
* showpersonsactivity yoanna
* showallteams
* showteamsactivity ninjasteamrules
* showteamsactivity svirepi
* showteamsactivity luvici
* showteammembers ninjasteamrules
* showteammembers svirepi
* showteammembers luvici
* showteamboards ninjasteamrules
* showteamboards svirepi
* showteamboards luvici
* showboardsactivity boarddata  ninjasteamrules
* showboardsactivity mnogosme svirepi
* showboardsactivity gladnite luvici
* listbugitems
* liststoryitems
* listsfeedbackitems
* listitemsbystatus done
* listitemsbystatus active
* listsortbypriority
* listsorttitle
* listsortseverity
* listsortsize
* listfeedrating
* listbystatusandassignee active yoanna
* listbystatusandassignee fixed tomas
* listallworkitems
* listbyassignee
* showallpeople


**WorkItem Expected Output Data**
* createmember tomas
* Member with the name: tomas was created.
* ####################
* createmember asenka
* Member with the name: asenka was created.
* ####################
* createmember yoanna
* Member with the name: yoanna was created.
* ####################
* createteam ninjasteamrules
* Team with the name: ninjasteamrules was created.
* ####################
* createteam svirepi
* Team with the name: svirepi was created.
* ####################
* createteam luvici
* Team with the name: luvici was created.
* ####################
* createboard boarddata ninjasteamrules
* Board with the name: boarddata was created.
* ####################
* createboard mnogosme svirepi
* Board with the name: mnogosme was created.
* ####################
* createboard gladnite luvici
* Board with the name: gladnite was created.
* ####################
* addpersontoteam tomas ninjasteamrules
* Member: tomas has been added to team: ninjasteamrules successfully.
* ####################
* addpersontoteam asenka svirepi
* Member: asenka has been added to team: svirepi successfully.
* ####################
* addpersontoteam yoanna luvici
* Member: yoanna has been added to team: luvici successfully.
* ####################
* createbuginboard tozigomaxam boarddata ninjasteamrules fixed critical high toma is a very good developer
* Describe the issue number: 1.Please type 'finish' when you are ready.
* pisna mi!
* Step has been added sucessfully
* Describe the issue number: 2.Please type 'finish' when you are ready.
* oshte malko!
* Step has been added sucessfully
* Describe the issue number: 3.Please type 'finish' when you are ready.
* finish
* Steps to reproduce has been sucessfully submitted.
* Bug id 1: tozigomaxam was successfully created.
* ####################
* createbuginboard tomaegotin boarddata ninjasteamrules fixed critical high toma is a very good developer
* Describe the issue number: 1.Please type 'finish' when you are ready.
* otivam na plaja!
* Step has been added sucessfully
* Describe the issue number: 2.Please type 'finish' when you are ready.
* na bara sum, sipvai!
* Step has been added sucessfully
* Describe the issue number: 3.Please type 'finish' when you are ready.
* finish
* Steps to reproduce has been sucessfully submitted.
* Bug id 2: tomaegotin was successfully created.
* ####################
* createbuginboard asenegotin mnogosme svirepi fixed critical high asen is a very good developer
* Describe the issue number: 1.Please type 'finish' when you are ready.
* pushi mi se nargile!
* Step has been added sucessfully
* Describe the issue number: 2.Please type 'finish' when you are ready.
* pushih veche!
* Step has been added sucessfully
* Describe the issue number: 3.Please type 'finish' when you are ready.
* finish
* Steps to reproduce has been sucessfully submitted.
* Bug id 3: asenegotin was successfully created.
* ####################
* createbuginboard yoannaegotina gladnite luvici fixed critical high yoanna is a very good developer
* Describe the issue number: 1.Please type 'finish' when you are ready.
* gladna sum!
* Step has been added sucessfully
* Describe the issue number: 2.Please type 'finish' when you are ready.
* izqdoh surnata!
* Step has been added sucessfully
* Describe the issue number: 3.Please type 'finish' when you are ready.
* finish
* Steps to reproduce has been sucessfully submitted.
* Bug id 4: yoannaegotina was successfully created.
* ####################
* assignworkitemtoperson boarddata ninjasteamrules 1 tomas
* Item tozigomaxam has been successfully assigned to tomas from board boarddata.
* ####################
* unassignworkitemtoperson boarddata ninjasteamrules 1 tomas
* Item tozigomaxam has been successfully unassigned to tomas from board boarddata.
* ####################
* assignworkitemtoperson boarddata ninjasteamrules 2 tomas
* Item tomaegotin has been successfully assigned to tomas from board boarddata.
* ####################
* assignworkitemtoperson mnogosme svirepi 3 asenka
* Item asenegotin has been successfully assigned to asenka from board mnogosme.
* ####################
* assignworkitemtoperson gladnite luvici 4 yoanna
* Item yoannaegotina has been successfully assigned to yoanna from board gladnite.
* ####################
* createfeedbackinboard feedbackfromAsen new 5 boarddata ninjasteamrules this project is awesome
* Feedback id 5: feedbackfromasen was successfully created.
* ####################
* assignworkitemtoperson boarddata ninjasteamrules 5 tomas
* This is just a Feedback. It cannot be assign to tomas.
* ####################
* createfeedbackinboard feedbackfromToma new 5 mnogosme svirepi this project is amazing
* Feedback id 6: feedbackfromtoma was successfully created.
* ####################
* assignworkitemtoperson mnogosme svirepi 6 asenka
* This is just a Feedback. It cannot be assign to asenka.
* ####################
* createfeedbackinboard feedbackfromYoanna new 5 gladnite luvici idavame po mnogo
* Feedback id 7: feedbackfromyoanna was successfully created.
* ####################
* assignworkitemtoperson gladnite luvici 7 yoanna
* This is just a Feedback. It cannot be assign to yoanna.
* ####################
* createstoryinboard morningstar small high done boarddata ninjasteamrules lucifer morning star
* Story id 8: morningstar was successfully created.
* ####################
* assignworkitemtoperson boarddata ninjasteamrules 8 tomas
* Item morningstar has been successfully assigned to tomas from board boarddata.
* ####################
* createstoryinboard sunriseincuba small high done gladnite luvici watching the sinrise on the beach
* Story id 9: sunriseincuba was successfully created.
* ####################
* assignworkitemtoperson gladnite luvici 9 yoanna
* Item sunriseincuba has been successfully assigned to yoanna from board gladnite.
* ####################
* createstoryinboard sunsetinmalibu small high done mnogosme svirepi eating what we can find
* Story id 10: sunsetinmalibu was successfully created.
* ####################
* assignworkitemtoperson mnogosme svirepi 10 asenka
* Item sunsetinmalibu has been successfully assigned to asenka from board mnogosme.
* ####################
* addcommenttoworkitem 2 tomas bug is bigger than expected
* Member: tomas has added a comment: bug is bigger than expected to the work item.
* ####################
* addcommenttoworkitem 8 tomas When the sun rise - up, it's time for action!
* Member: tomas has added a comment: when the sun rise - up, it's time for action! to the work item.
* ####################
* addcommenttoworkitem 3 asenka tabacco is almost over
* Member: asenka has added a comment: tabacco is almost over to the work item.
* ####################
* addcommenttoworkitem 10 asenka Welcome to Miami!
* Member: asenka has added a comment: welcome to miami! to the work item.
* ####################
* addcommenttoworkitem 4 yoanna I forgot that I am a vegeterian
* Member: yoanna has added a comment: i forgot that i am a vegeterian to the work item.
* ####################
* addcommenttoworkitem 9 yoanna My vacation in Cuba is great!
* Member: yoanna has added a comment: my vacation in cuba is great!to the work item.
* ####################
* changebugpriority 2 medium
* Bug Priority with title: tomaegotin was updated to priority status: Medium
* ####################
* changebugseverity 3 major
* Bug Severity with title: asenegotin was updated to severity status: Major
* ####################
* changebugstatus 4 active
* Bug Status with title: yoannaegotina was updated to status: Active
* ####################
* changefeedbackstatus 5 scheduled
* Feedback status with title: feedbackfromasen was updated to feed: Scheduled
* ####################
* changefeedbackrating 6 4
* Feedback rating with title: feedbackfromtoma was updated to rating: 4
* ####################
* changefeedbackrating 7 4
* Feedback rating with title: feedbackfromyoanna was updated to rating: 4
* ####################
* changestorystatus 8 inprogress
* Story Status with ID: 8, title: morningstar was updated to status: InProgress
* ####################
* changestorysize 9 large
* Story Size with title: sunriseincuba was updated to size: Large
* ####################
* changestorypriority 10 medium
* Story Priority with title: sunsetinmalibu was updated to priority: Medium
* ####################
* showpersonsactivity tomas
* Member with the name: tomas was created.
* Added on: 09 / 08 / 20 22:51:23
*  tomas has been added to team ninjasteamrules.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: tozigomaxam.
* Added on: 09 / 08 / 20 22:51:23
*  The item tozigomaxam of tomas from boarddata has been unnasigned.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: tomaegotin.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: morningstar.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showpersonsactivity asenka
* Member with the name: asenka was created.
* Added on: 09 / 08 / 20 22:51:23
*  asenka has been added to team svirepi.
* Added on: 09 / 08 / 20 22:51:23
*  asenka from mnogosme has recieved an item: asenegotin.
* Added on: 09 / 08 / 20 22:51:23
*  asenka from mnogosme has recieved an item: sunsetinmalibu.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showpersonsactivity yoanna
* Member with the name: yoanna was created.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna has been added to team luvici.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna from gladnite has recieved an item: yoannaegotina.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna from gladnite has recieved an item: sunriseincuba.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showallteams
* ninjasteamrules
* svirepi
* luvici
* ####################
* showteamsactivity ninjasteamrules
* Team with the name: ninjasteamrules was created.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showteamsactivity svirepi
* Team with the name: svirepi was created.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showteamsactivity luvici
* Team with the name: luvici was created.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showteammembers ninjasteamrules
* ------------------------
* Member Name: tomas
* ------------------------
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
*  Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* ------
* History:
* ------
* Member with the name: tomas was created.
* Added on: 09 / 08 / 20 22:51:23
*  tomas has been added to team ninjasteamrules.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: tozigomaxam.
* Added on: 09 / 08 / 20 22:51:23
*  The item tozigomaxam of tomas from boarddata has been unnasigned.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: tomaegotin.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: morningstar.
* Added on: 09 / 08 / 20 22:51:23
* 
* 
* ####################
* showteammembers svirepi
* ------------------------
* Member Name: asenka
* ------------------------
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
*  Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* 
* ------
* History:
* ------
* Member with the name: asenka was created.
* Added on: 09 / 08 / 20 22:51:23
*  asenka has been added to team svirepi.
* Added on: 09 / 08 / 20 22:51:23
*  asenka from mnogosme has recieved an item: asenegotin.
* Added on: 09 / 08 / 20 22:51:23
*  asenka from mnogosme has recieved an item: sunsetinmalibu.
* Added on: 09 / 08 / 20 22:51:23
* 
* 
* ####################
* showteammembers luvici
* ------------------------
* Member Name: yoanna
* ------------------------
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
*  Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* ------
* History:
* ------
* Member with the name: yoanna was created.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna has been added to team luvici.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna from gladnite has recieved an item: yoannaegotina.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna from gladnite has recieved an item: sunriseincuba.
* Added on: 09 / 08 / 20 22:51:23
* 
* 
* ####################
* showteamboards ninjasteamrules
* boarddata
* ####################
* showteamboards svirepi
* mnogosme
* ####################
* showteamboards luvici
* gladnite
* ####################
* showboardsactivity boarddata  ninjasteamrules
* Board with name:boarddata was created.
* Added on: 09 / 08 / 20 22:51:23
*  boarddata with tozigomaxam has been assigned to tomas.
* Added on: 09 / 08 / 20 22:51:23
*  boarddata with tozigomaxam has been unassigned to tomas.
* Added on: 09 / 08 / 20 22:51:23
*  boarddata with tomaegotin has been assigned to tomas.
* Added on: 09 / 08 / 20 22:51:23
*  boarddata with morningstar has been assigned to tomas.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showboardsactivity mnogosme svirepi
* Board with name:mnogosme was created.
* Added on: 09 / 08 / 20 22:51:23
*  mnogosme with asenegotin has been assigned to asenka.
* Added on: 09 / 08 / 20 22:51:23
*  mnogosme with sunsetinmalibu has been assigned to asenka.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* showboardsactivity gladnite luvici
* Board with name:gladnite was created.
* Added on: 09 / 08 / 20 22:51:23
*  gladnite with yoannaegotina has been assigned to yoanna.
* Added on: 09 / 08 / 20 22:51:23
*  gladnite with sunriseincuba has been assigned to yoanna.
* Added on: 09 / 08 / 20 22:51:23
* 
* ####################
* listbugitems
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* 
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
* 
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* ####################
* liststoryitems
* Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* ####################
* listsfeedbackitems
* Type: Feedback
* Work Item ID: 5
* Title: feedbackfromasen
* Description: this project is awesome
* ---Feedback Info-- -
* Feedback status: Scheduled
* Feedback Rating: 5
* 
* Type: Feedback
* Work Item ID: 6
* Title: feedbackfromtoma
* Description: this project is amazing
* ---Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* 
* Type: Feedback
* Work Item ID: 7
* Title: feedbackfromyoanna
* Description: idavame po mnogo
* -- - Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* ####################
* listitemsbystatus done
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* ####################
* listitemsbystatus active
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* ####################
* listsortbypriority
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
* 
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* 
* Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* ####################
* listsorttitle
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
* 
* Type: Feedback
* Work Item ID: 5
* Title: feedbackfromasen
* Description: this project is awesome
* ---Feedback Info-- -
* Feedback status: Scheduled
* Feedback Rating: 5
* 
* Type: Feedback
* Work Item ID: 6
* Title: feedbackfromtoma
* Description: this project is amazing
* ---Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* 
* Type: Feedback
* Work Item ID: 7
* Title: feedbackfromyoanna
* Description: idavame po mnogo
* -- - Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* 
* Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* 
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* ####################
* listsortseverity
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
* 
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* 
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* ####################
* listsortsize
* Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* 
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* ####################
* listfeedrating
* Type: Feedback
* Work Item ID: 5
* Title: feedbackfromasen
* Description: this project is awesome
* ---Feedback Info-- -
* Feedback status: Scheduled
* Feedback Rating: 5
* 
* Type: Feedback
* Work Item ID: 6
* Title: feedbackfromtoma
* Description: this project is amazing
* ---Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* 
* Type: Feedback
* Work Item ID: 7
* Title: feedbackfromyoanna
* Description: idavame po mnogo
* -- - Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* ####################
* listbystatusandassignee active yoanna
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* ####################
* listbystatusandassignee fixed tomas
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* ####################
* listallworkitems
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* 
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
* 
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* 
* Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* 
* Type: Feedback
* Work Item ID: 5
* Title: feedbackfromasen
* Description: this project is awesome
* ---Feedback Info-- -
* Feedback status: Scheduled
* Feedback Rating: 5
* 
* Type: Feedback
* Work Item ID: 6
* Title: feedbackfromtoma
* Description: this project is amazing
* ---Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* 
* Type: Feedback
* Work Item ID: 7
* Title: feedbackfromyoanna
* Description: idavame po mnogo
* -- - Feedback Info-- -
* Feedback status: New
* Feedback Rating: 4
* ####################
* listbyassignee
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
* 
* Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* 
* Type: Bug
* Work Item ID: 1
* Title: tozigomaxam
* Description: toma is a very good developer
* Comment from the author:
* ---Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* pisna mi!
* oshte malko!
* ------
* 
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
* 
* Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
* 
* Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* ####################
* showallpeople
* ------------------------
* Member Name: tomas
* ------------------------
* Type: Bug
* Work Item ID: 2
* Title: tomaegotin
* Description: toma is a very good developer
* Comment from the author: tomas
* Has added the following description: bug is bigger than expected
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Critical
* Bug Priority: Medium
* ------
* Assignee: tomas
* ------
* StepsToReproduce:
* otivam na plaja!
* na bara sum, sipvai!
* ------
*  Type: Story
* Work Item ID: 8
* Title: morningstar
* Description: lucifer morning star
* Comment from the author: tomas
* Has added the following description: when the sun rise - up, it's time for action!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: High
* Story Status: InProgress
* Assignee: tomas
* 
* ------
* History:
* ------
* Member with the name: tomas was created.
* Added on: 09 / 08 / 20 22:51:23
*  tomas has been added to team ninjasteamrules.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: tozigomaxam.
* Added on: 09 / 08 / 20 22:51:23
*  The item tozigomaxam of tomas from boarddata has been unnasigned.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: tomaegotin.
* Added on: 09 / 08 / 20 22:51:23
*  tomas from boarddata has recieved an item: morningstar.
* Added on: 09 / 08 / 20 22:51:23
* 
* 
* ------------------------
* Member Name: asenka
* ------------------------
* Type: Bug
* Work Item ID: 3
* Title: asenegotin
* Description: asen is a very good developer
* Comment from the author: asenka
* Has added the following description: tabacco is almost over
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Fixed
* Bug Severity: Major
* Bug Priority: High
* ------
* Assignee: asenka
* ------
* StepsToReproduce:
* pushi mi se nargile!
* pushih veche!
* ------
*  Type: Story
* Work Item ID: 10
* Title: sunsetinmalibu
* Description: eating what we can find
* Comment from the author: asenka
* Has added the following description: welcome to miami!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Small
* Story Priority: Medium
* Story Status: Done
* Assignee: asenka
* 
* ------
* History:
* ------
* Member with the name: asenka was created.
* Added on: 09 / 08 / 20 22:51:23
*  asenka has been added to team svirepi.
* Added on: 09 / 08 / 20 22:51:23
*  asenka from mnogosme has recieved an item: asenegotin.
* Added on: 09 / 08 / 20 22:51:23
*  asenka from mnogosme has recieved an item: sunsetinmalibu.
* Added on: 09 / 08 / 20 22:51:23
* 
* 
* ------------------------
* Member Name: yoanna
* ------------------------
* Type: Bug
* Work Item ID: 4
* Title: yoannaegotina
* Description: yoanna is a very good developer
* Comment from the author: yoanna
* Has added the following description: i forgot that i am a vegeterian
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Bug Info-- -
* Bug Status: Active
* Bug Severity: Critical
* Bug Priority: High
* ------
* Assignee: yoanna
* ------
* StepsToReproduce:
* gladna sum!
* izqdoh surnata!
* ------
*  Type: Story
* Work Item ID: 9
* Title: sunriseincuba
* Description: watching the sinrise on the beach
* Comment from the author: yoanna
* Has added the following description: my vacation in cuba is great!
* It was added at: 09 / 08 / 20 22:51:23
* 
* -- - Story Info-- -
* Story Size: Large
* Story Priority: High
* Story Status: Done
* Assignee: yoanna
* 
* ------
* History:
* ------
* Member with the name: yoanna was created.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna has been added to team luvici.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna from gladnite has recieved an item: yoannaegotina.
* Added on: 09 / 08 / 20 22:51:23
*  yoanna from gladnite has recieved an item: sunriseincuba.
* Added on: 09 / 08 / 20 22:51:23
* ####################